//
// Created by Microsoft on 5/3/2016.
//
#include "Format3.h"

#define MEMSIZE (1 << 20)

std::string Format3::process(Instruction &instruction, bool &isBase, int &baseVal) {
    if(instruction.getMnemonic() == "RSUB"){
        return "4F0000";
    }
    std::string first, second, third;
    first = Format::formFirstTwoBits(instruction);

    third = Format3::formAddressBits(instruction, isBase, baseVal);
    second = Format3::formThirdBit(instruction);
    //std::cout << "first " << first <<" second " <<second << " third " << third << std::endl;
    return first + second + third;
}

std::string Format3::formThirdBit(Instruction &instruction) {

    std::string thirdBitHexa;
    std::string firstBit;
    std::string secondBit;
    std::string thirdBit;
    std::string fourthBit = "0";
    if (instruction.isIndexed())
        firstBit = "1";
    else
        firstBit = "0";
    if (b == 1)
        secondBit = "1";
    else
        secondBit = "0";
    if (p == 1)
        thirdBit = "1";
    else
        thirdBit = "0";

    firstBit += secondBit;
    firstBit += thirdBit;
    firstBit += fourthBit;
    if (instruction.isStar())
        firstBit = "0010";
    thirdBitHexa = Format::fromBinaryStringToHexaString(firstBit);

    return thirdBitHexa;
}

std::string Format3::formAddressBits(Instruction &instruction, bool isBase, int baseVal) {

    std::string address;

   if(symTabAbs.find(instruction.getOperand()[0]) != symTabAbs.end() ){
      int temp  = symbolTable[instruction.getOperand()[0]];
       if(temp < 0 && !instruction.isImmediate()  )
           Format:: printError("Negative value in address",instruction) ;
       else{
           instruction.setAbsolute(true);
   }}
    if (instruction.isStar())
        address = "000";
    else if ( (!instruction.isNum()  || instruction.isExpression() ) && !instruction.isAbsolute() ) {

        int temp = atoi(instruction.getOperand()[0].c_str());
        std::map<std::string, int>::iterator itr;
        int isLabel = -1;

        if (!instruction.isNum())
            isLabel = 0;

        itr = symbolTable.find(instruction.getOperand()[0]);
        if (itr != symbolTable.end()) {// find label in symbol table
            temp = itr->second;
            isLabel = 1;
        }
        if (isLabel == 0) {
            Format::printError("Can't find label in symbol table", instruction);
        }
        int targetAddress = temp - (instruction.getLocctr() + 3);
//            std::cout << "targetAddress" << targetAddress << std::endl;

        if ((targetAddress >= -2048) && (targetAddress <= 2047)) { /// FFF in hex
            address = Format::fromIntToHex(targetAddress);
            p = 1;
        }
        else if (isBase) {

            targetAddress = temp - baseVal;
            if ((targetAddress <= 4095) && (targetAddress >= 0)) {
                address = Format::fromIntToHex(targetAddress);
                b = 1;
            }
            else {
                Format::printError("Memory not enough", instruction);
            }
        }
        else {
            Format::printError("Memory not enough", instruction);
        }
    }

    else if ((instruction.isImmediate() || instruction.isIndirect() ) && instruction.isNum()) {
        std::string temp = instruction.getOperand()[0];
        address = Format::fromIntToHex(Format::fromStringToInt(temp));
    }
    else if (instruction.isAbsolute()) {
        if (instruction.isExpression()) {
            if(Format::fromStringToInt(instruction.getOperand()[0])<0 && !instruction.isImmediate()){
                Format::printError("Invalid Address",instruction);
                return "000";
            }
            std::string temp = instruction.getOperand()[0];
            address = Format::fromIntToHex(Format::fromStringToInt(temp));
            if (!(address.length() <= 3)) {
                Format::printError("Address cant fit in format 3 ", instruction);
                return "000";
            }
        }
        else {
            std::string temp = instruction.getOperand()[0];
            int temp2 ;
            if(!instruction.isNum()){
               temp2 = symbolTable[temp];
                address = Format::fromIntToHex(temp2) ;
                if(address.size() > 3) {
                    Format::printError("Address cant fit in format 3 ", instruction);
                    return "000";
                }
                return Format::validAddress(3, address);
            }
            if (temp.length() <= 3)
                address = temp;

            else
                Format::printError("Address cant fit in format 3 ", instruction);
        }
    }
    return Format::validAddress(3, address);
}