#ifndef PARSER_H
#define PARSER_H

#include "Instruction.h"

class Parser {

private:
    void prepareInstruction(std::string fullRecord);
    bool isComment(std::string s);
    bool isBlank(std::string s);
    void parseAppendix(std::string file);
    std::vector<std::string> splitAscii(std::string str,std::regex rgx);
    std::map <std::string, std::vector < std::string >> asciiMap;
    std::map <std::string , int > opCodeNumeric;
    std::map <std::string , std::string > opCode;
    std::map <std::string , std::vector < std::string> > fullAppendix;
    std::map <std::string, int > registers;
    std::map <std::string, int > directives;
    std::vector <std::string> recordBuffer;
    std::vector <std::string> instructionsAsItIs;
    std::vector <std::string> labels;
    std::string label;
    std::string mnemonic;
    std::vector <std::string> operand;
    std::vector <std::string> errorVector;
    bool isOperation;
    bool indexed;
    bool immediate;
    bool indirect;
    bool star;
    bool absolute;
    bool num;
    bool expression;
    bool litteral;
    bool externalRef;
    bool simple;
    std::string comment;
    int recordIndex;
    int format = 0;
    int fileSize;
    int tokensNumber;

public :
    Parser();
    void loadAppendix();
    Instruction getInstruction();
    std::vector<std::string> readFile(std::string file);
    int getFileSize();
    std::map<std::string, std::vector < std::string> > getFullAppendix();
    std::map<std::string, std::string> getOpCode();
    std::map <std::string, int > getRegisters();
    void parseAscii(std::string fileName);
    std::map <std::string, std::vector < std::string > > getAsciiMap();
};

#endif //PARSER_H
