#ifndef PASS1_PASS2_H
#define PASS1_PASS2_H
#include "Instruction.h"
#include "Format.h"
#include "Format2.h"
#include "Format3.h"
#include "Format4.h"
#include "GenerateRecord.h"
#include "GeneralExpression.h"

class Pass2 {

private:
    std::vector<Instruction> instructions;
    std::string objectCode;
    std::vector <std::string> errorVector;
    std::vector <std::string> header;
    std::map <std::string, std::vector < std::string > > asciiMap;
    bool baseUsed;
    bool startFound;
    bool dummyFound;
    int baseValue;
    std::map<std::string , int> symbolTable;
    int power(int base, int power);
    int stringToInt(std::string str);
    std::vector <Instruction> instructionVector;
    std::map <std::string, std::string> opcode;
    std::map <std::string, int > registers;
    std::vector<std::string> externalReference;
    std::vector<std::pair<std::string, std::string>> extDefs;
    std::string decimalToHexa(int decimalNumber);
    int locctr;
    std::string programName;
    int startAddress;
    int firstExeAddress;
    GeneralExpression generalExpression ;
    std::map <std::string, std::string> extExp;
    std::map <std::string, bool> symTabAbs;

    void print(Instruction instruction) ;

public:
    Pass2(std::map<std::string, std::string> opcode, std::vector<Instruction> instructions,
          std::map<std::string, int> symbolTable, std::map<std::string, int> registers,
          std::vector<std::string> externalReference, std::map<std::string, std::vector<std::string> > asciiMap,
          std::map<std::string, int> littab, std::map <std::string, bool> symTabAbs);
    std::vector<Instruction> processInstructions();
    void writeObjectProgram();
};

#endif //PASS1_PASS2_H