#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <bits/stdc++.h>

class Instruction
{

private:
    bool isOperation;
    bool indexed;
    bool immediate;
    bool indirect;
    bool num;
    bool expression;
    bool externalRef;
    bool litteral;
    bool simple;
    bool absolute;
    bool star;
    int format;
    int locctr;
    std::string objcode;
    std::string comment;
    std::string record;
    std::string label;
    std::string mnemonic;
    std::vector<std::string> operand;
    std::vector <std::string> errorVector;
    std::vector<std::pair<std::string, std::string>> extRefs ;

public:


    Instruction(bool simple, std::string record, std::vector<std::string> operand,
                std::string objcode, bool num, std::string mnemonic, int locctr, bool litteral,
                std::string label, bool isOperation, bool indirect, bool indexed, bool immediate, int format,
                bool externalRef, bool expression, std::vector<std::string> errorVector,
                std::string comment, bool absolute,bool star);

    std::vector<std::pair<std::string, std::string>> getExternalReference ();
    bool isAbsolute();
    bool isStar();
    void setIsNum (bool isNum);
    void setAbsolute(bool absolute);

    std::string getComment();

    void setComment(std::string comment);

    std::vector<std::string> getErrorVector();

    void setErrorVector(std::vector<std::string> errorVector);

    bool isExpression();

    void setExpression(bool expression);

    bool isExternalRef();

    void setExternalRef(bool externalRef);

    int getFormat();

    void setFormat(int format);

    bool isImmediate();

    void setImmediate(bool immediate);

    bool isIndexed();

    void setIndexed(bool indexed);

    bool isIndirect();

    void setIndirect(bool indirect);

    bool isIsOperation();

    void setIsOperation(bool isOperation);

    std::string getLabel();

    void setLabel(std::string label);

    bool isLitteral();

    void setLitteral(bool litteral);

    int getLocctr();

    void setLocctr(int locctr);

    std::string getMnemonic();

    void setMnemonic(std::string mnemonic);

    bool isNum();

    void setNum(bool num);

    std::string getObjcode();

    void setObjcode(std::string objcode);

    std::vector<std::string> getOperand();

    void setOperand(std::vector<std::string> operand);

    std::string getRecord();

    void setRecord(std::string record);

    bool isSimple();

    void setSimple(bool simple);

};

#endif // INSTRUCTION_H