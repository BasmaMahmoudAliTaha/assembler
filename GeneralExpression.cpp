#include "GeneralExpression.h"
#include <regex>
#include <bits/stdc++.h>
#include <stdlib.h>

GeneralExpression::GeneralExpression(){
  this->absolute = true;
  this->error = false;
}
inline int intDivEx (long long numerator, long long denominator) {
  if (denominator == 0)
    throw std::overflow_error("Divide by zero exception");
  return numerator / denominator;
}

bool GeneralExpression::isHex(std::string operand){
  std::regex decimal("(\\d+)");
  std::regex hex("[0-9a-fA-F]+");
  if(std::regex_match(operand, decimal) || std::regex_match(operand, hex)){
    return true;
  }
  return false;
}
/*
 * checks validity of absolute expression
 * and calls internally a funcion to
 * calculate expression value
 */
bool GeneralExpression::validAbsOrRelative(){
  try{
    if(pairsWithOppositeSign()){ // can be absolute of relative, an d is determined internally in the function
      return true;
    }else if(isHex(operand)){
      std::istringstream buffer(operand);
      buffer >> std::hex >> valueOfExp; // value now contains the decimal number of the hex string
      return true;
    }else if(digitsExp()){
      return true;
    }else if(mixBetnSymAndConst()){// can be absolute of relative, and is determined internally in the function
      return true;
    }
    error = true;
    return false;
  }catch(std::overflow_error e){
    error = true;
  }

}

std::vector<std::string> GeneralExpression::split(std::string str,std::regex rgx){
  std::sregex_token_iterator iter(str.begin(),str.end(),rgx,-1);
  std::sregex_token_iterator end;
  std::vector<std::string> ans;
  int i = 0;
  for ( ; iter != end; ++iter){
    ans.push_back(*iter);
    ++i;
  }
  if(ans.at(0).compare("") == 0){
    ans.erase(ans.begin());
  }
  return ans;
}

void GeneralExpression::calculateExp(){

  long long value = 0, perecedingRes = 1;
  bool pereced = false;
  int counter = 0;

  for(int i = 0; i < terms.size(); ++i){

    std::string label = terms.at(i);

    if(symTable.find(label) != symTable.end()){ // label is in symbol table

      int address = symTable.find(label)->second;

      if(operations.at(i).compare("+") == 0){

        if((i+1 < operations.size() && (operations.at(i+1).compare("*")==0||operations.at(i+1).compare("/")==0))){

          pereced = true;
        }
        else{

          if(perecedingRes != 1){

            value += perecedingRes;
            perecedingRes = 1;
            counter = 0;
          }

          value += address;
        }

      }
      else if(operations.at(i).compare("-") == 0){

        if((i+1 < operations.size() && (operations.at(i+1).compare("*")==0||operations.at(i+1).compare("/")==0))){

          pereced = true;
        }
        else{

          if(perecedingRes != 1){

            value += perecedingRes;
            perecedingRes = 1;
            counter = 0;
          }

          value -= address;
        }
      }
      else if(operations.at(i).compare("*") == 0 && symTableAbs.find(terms.at(i)) != symTableAbs.end()){

        int temp = symTable.find(terms.at(i))->second;
        counter++;
        updatePereceding(counter, perecedingRes,temp,i,"*");
        if(i+1 < operations.size() && (operations.at(i+1).compare("*") != 0 || operations.at(i+1).compare("/") != 0)){

          continue;
        }
        else{
          evaluateValue(counter, value,perecedingRes,pereced,i);
        }
      }
      else if(operations.at(i).compare("/") == 0 && symTableAbs.find(terms.at(i)) != symTableAbs.end()){
        int temp = symTable.find(terms.at(i))->second;
        counter++;
        updatePereceding(counter, perecedingRes,temp,i,"/");

        if(i+1 < operations.size() && (operations.at(i+1).compare("*") != 0 || operations.at(i+1).compare("/") != 0)){

          continue;
        }else{

          evaluateValue(counter, value,perecedingRes,pereced,i);
        }

      }else{
        error = true;
      }
    }else{ // absolute value

      if(isHex(label)){

        int temp;
        std::istringstream buffer(label);
        buffer >> std::hex >> temp; // value now contains the decimal number of the hex string

        if(operations.at(i).compare("+") == 0 ){

          if((i+1 < operations.size() && (operations.at(i+1).compare("*")==0||operations.at(i+1).compare("/")==0))){

            pereced = true;
          }
          else{
            if(perecedingRes != 1){

              value += perecedingRes;
              perecedingRes = 1;
              counter = 0;
            }

            value += temp;
          }

        }
        else if(operations.at(i).compare("-") == 0){

          if((i+1 < operations.size() && (operations.at(i+1).compare("*")==0||operations.at(i+1).compare("/")==0))){

            pereced = true;
          }

          else{
            if(perecedingRes != 1){

              value += perecedingRes;
              perecedingRes = 1;
              counter = 0;
            }
            value -= temp;
          }

        }
        else if(operations.at(i).compare("*") == 0){

          evaluate(pereced,"*", i, perecedingRes, value, temp, counter);
        }
        else if(operations.at(i).compare("/") == 0){

          evaluate(pereced,"/", i, perecedingRes, value, temp, counter);
        }
        else{ // not valid operator

          error = true;
        }

      }
      else{ // not valid label

        error = true;
      }
    }
    if(error)
      break;
  }
  if(!error)
    valueOfExp = value;

}

void GeneralExpression::evaluate(bool &pereced,std::string operation,int i, long long &perecedingRes, long long &value, int temp, int &counter){

  if(!pereced && (symTableAbs.find(terms.at(i-1)) != symTableAbs.end()||isHex(terms.at(i-1)))){


    if(operation.compare("*") == 0){

      value *= temp;

    }
    else if(operation.compare("/") == 0){

      try{
        intDivEx(value, temp);
        value/=temp;
      }catch(std::overflow_error e){
        error = true;
      }

    }

  }
  else if(pereced && (symTableAbs.find(terms.at(i-1)) != symTableAbs.end()||isHex(terms.at(i-1)))){

    counter++;
    updatePereceding(counter, perecedingRes,temp,i,operation);

    if(!(i+1 < operations.size() && (operations.at(i+1).compare("*") != 0 || operations.at(i+1).compare("/") != 0))){

      evaluateValue(counter, value,perecedingRes,pereced,i);

    }
  }
  else {

    error = true;
  }
}

void GeneralExpression::evaluateValue(int counter, long long &value, long long &perecedingRes,bool &pereced, int i){

  if(operations.at(i-counter).compare("+") == 0){

    value+=perecedingRes;

  }else if(operations.at(i-counter).compare("-") == 0){

    value-=perecedingRes;

  }else {

    error = true;
  }

  pereced = false;
  perecedingRes = 1;
}

void GeneralExpression::updatePereceding(int &counter, long long &perecedingRes, int temp, int i, std::string operation){
  int thePrevLabel;
  if(counter > 1){

    if(operation.compare("*") == 0){

      perecedingRes *= temp;

    }
    else if(operation.compare("/") == 0){
      try{
        intDivEx(perecedingRes, temp);
        perecedingRes /= temp;
      }catch(std::overflow_error e){
        error = true;
      }

    }

  }else{
    if(symTableAbs.find(terms.at(i-1)) != symTableAbs.end()){ //it is an absolute label

      thePrevLabel = symTable.find(terms.at(i-1))->second;

    }else if(symTable.find(terms.at(i-1)) != symTable.end()){
      error = true;
      return;
    }else{

      std::istringstream buffer(terms.at(i-1));
      buffer >> std::hex >> thePrevLabel; // value now contains the decimal number of the hex string

    }
    if(operation.compare("*") == 0){

      perecedingRes = thePrevLabel*temp;

    }
    else if(operation.compare("/") == 0){
      try{
        intDivEx(thePrevLabel, temp);
        perecedingRes = thePrevLabel/temp;
      }catch(std::overflow_error e){
        error = true;
      }
    }

  }

}

bool GeneralExpression::mixBetnSymAndConst(){
  std::regex mix("([*+-/]((0[0-9A-Fa-f]*)|([A-Za-z]\\w*)))+");
  std::smatch match;

  if(std::regex_match(operand,match,mix) && match.size() > 1) {

    labels = countSymbols();

    subtractAbsLabels();
    if (labels % 2 == 0) { // even labels with absolute value gives an absolute

      if (plus - minus == 0) {

        calculateExp();
        return !error;

      } else {

        return false;
      }
    } else {

      if (plus - minus == 1) { // odd labels with the remaining sign positive

        absolute = false; // relative expression
        calculateExp();
        return !error;

      }

      return false;

    }
  }else
    return false;
}

void GeneralExpression::subtractAbsLabels(){

  for(int i = 0; i < terms.size(); ++i){

    if(symTableAbs.find(terms.at(i))!= symTableAbs.end()) {
      --labels;
      if (operations.at(i).compare("+") == 0)
        --plus;
      else if (operations.at(i).compare("-") == 0)
        --minus;
    }
  }
}

int GeneralExpression::countSymbols(){
  int labels = 0;
  for(int i = 0; i < operand.size(); ++i){
    if(i+1 < operand.size() && operand.at(i) == '+' && i!= operand.size()-1 && isalpha(operand.at(i+1))){

      plus++;
      labels++;
      operations.push_back("+");

    }else if(i+1 < operand.size() && operand.at(i) == '-'&& i!= operand.size()-1 && isalpha(operand.at(i+1))) {

      minus++;
      operations.push_back("-");
      labels++;

    }else if(operand.at(i) == '*' &&  i!= operand.size()-1 && isalpha(operand.at(i+1))){

      labels++;
      operations.push_back("*");

    }else if(operand.at(i) == '/' &&  i!= operand.size()-1 && isalpha(operand.at(i+1))){

      labels++;
      operations.push_back("/");

    }else if(operand.at(i) == '+'){

      operations.push_back("+");

    }else if(operand.at(i) == '-'){

      operations.push_back("-");

    }else if(operand.at(i) == '*'){

      operations.push_back("*");

    }else if(operand.at(i) == '/'){

      operations.push_back("/");
    }
  }
  return labels;
}
bool GeneralExpression::pairsWithOppositeSign(){
  std::regex exp("([*+-/][A-Za-z]\\w*)+");
  std::smatch match;

  if(std::regex_match(operand,match,exp)){
    labels = countSymbols();
    subtractAbsLabels();

    if(labels % 2 == 0){

      if(plus - minus == 0){

        calculateExp();
        return !error;
      }else{
        return false;
      }
    }else {
      if(plus - minus == 1){ // only a positive label is remained.

        calculateExp();
        absolute = false;
        return !error;

      }

      return false;
    }

  }
  else{
    return false;
  }

}

bool GeneralExpression::digitsExp(){

  std::regex exp("([*+-/][0-9A-Fa-f])+");
  std::smatch match;

  if(std::regex_match(operand,match,exp)){

    countSymbols();
    calculateExp();
    return !error;

  }else
    return false;
}

std::pair<bool, long long> GeneralExpression::getValue(std::string operand){

  this->operand = operand;
  this->absolute = true;
  this->error = false;
  minus = 0, plus = 0, labels = 0;
  valueOfExp = 0;
  this->operations.clear();
  this->terms.clear();

  if(operand.at(0)!='-'){
    this->operand = "+" + this->operand;
  }

  std::regex reg("[*+-/]");
  terms = split(operand,reg);
  std::pair<bool, long long> result;

  if(validAbsOrRelative()){
    result.first = false;
    result.second = valueOfExp;
    return result;

  } else{
    result.first = true; // error
    return result;
  }
}

bool GeneralExpression::isAbsoluteOrRel(){
  return absolute;
}

void GeneralExpression::setSymTableRelOrAbs(std::map<std::string, bool> symTableRelOrAbs) {
  this->symTableAbs = symTableRelOrAbs;

}

void GeneralExpression::setSymTable(std::map <std::string, int> symTable){
  this-> symTable = symTable;
}