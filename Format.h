//
// Created by Microsoft on 5/3/2016.
//

#ifndef PASS1_FORMAT_H
#define PASS1_FORMAT_H

#include <bits/stdc++.h>
#include <stdio.h>
#include "Instruction.h"
#include <bitset>
#include <sstream>

class Format {
public:
    Format(std::map<std::string, std::string> opcode, std::map<std::string, int> symbolTable,
           std::vector<std::string> externalReference, std::map<std::string, int> registers, std::map <std::string, bool> symTabAbs);   //constructor

    std::string process(Instruction &instruction, bool &isBase, int &baseVal);

protected:
    std::map<std::string, std::string> opcode;
    std::map<std::string, int> symbolTable;
    std::map<std::string, int> registers;
    std::map<std::string , bool > symTabAbs ;
    std::vector<std::string> externalReference;

    std::string formFirstTwoBits(Instruction &instruction);
    virtual std::string formAddressBits(Instruction &instruction, bool , int) = 0;
    virtual std::string formThirdBit(Instruction &instruction) = 0;

    std::string getOpcode(Instruction &instruction);
    std::string fromIntToHex(int add);
    int fromBinaryStringToInt(std::string str);
    void printError(std::string error, Instruction &instruction);
    int fromStringToInt(std::string str);
    std::string validAddress(int wantedSize, std::string str);
    std ::string fromBinaryStringToHexaString(std::string binary);
    std::string stringToUpper (std::string s) ;

};


#endif //PASS1_FORMAT_H
