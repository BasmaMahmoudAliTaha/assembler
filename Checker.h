#ifndef CHECKER_H
#define CHECKER_H
#include <bits/stdc++.h>

class Checker
{
public:
    Checker(std::map <std::string, int> opCode, std::map <std::string, int> registers,std::map <std::string, int> dirictives);
    bool isKeyWord(std::string field);
    bool isValidSyntax(std::string label);
    bool isValidAddress(std::string operand);
    bool isValidRegister(std::string operand);
    bool isValidForFormat3And4(std::string mnemonic,std::string operand);
    bool isNumeric(std::string operand);
    bool isLabel(std::string operand);
    int  validOperand(std::string mnemonic , std::string operand);
    bool isValidForFormatTwo(std::string mnemonic, std::string operand);
    bool findCharInString(std::string str , char c);
    bool isIndexed();
    bool isImmediate();
    bool isIndirect();
    bool isNum();
    bool isExpression();
    bool isExternalRef();
    bool isLitteral();
    bool isSimple();
    bool isAbsolute();
    bool isStar();
    void determineOperandType(std::string operand);
    void setLabels(std::vector<std::string> labels);
    std::vector<std::string> getOperand();
    bool isDirective(std::string operand);
    bool isHex(std::string operand);
    void resetFlags();
private:
    std::vector<std::string> labels;
    std::map <std::string, int > opCode;
    std::map <std::string, int > registers;
    std::map <std::string, int > directives;
    std::vector<std::string> operands;
    bool indexed,immediate,indirect,num ,expression ,litteral ,externalRef ,simpleVal, absolute, star;


};

#endif // CHECKER_H
