//
// Created by Microsoft on 5/6/2016.
//

#ifndef PASS1_GENRATERECORD_H
#define PASS1_GENRATERECORD_H
#include <bits/stdc++.h>
#include <stdio.h>
#include "Instruction.h"
#include<bitset>

#include <vector>
//#include <boost/algorithm/string.hpp>

class GenerateRecord {
public:
    GenerateRecord(std::pair<std::string, int> start, int end, int lastLocCtr, std::vector<std::string> extRefs,
                   std::vector<std::pair <std::string, std::string>> extDefs, std::vector<Instruction> instructions);
    void process();

private:
    std::vector<std::string> extRefs ;
    std::vector<std::pair <std::string, std::string>> extDefs;
    std::vector<Instruction> instructions;
    std::pair<std::string, int> start;
    std::vector <std::string> modificationRecords ;
    int end;
    int lastLocCtr;

    std::string decimalToHex(int x);
    std::string createHeader();
    std::string createEnd();
    std::string createDefs();
    std::string createRefs();
    std :: string  validAddress(int wantedSize , std::string str);
    std::vector<std::string> createTexts();
    void createModRecs(Instruction instruction);
};


#endif //PASS1_GENRATERECORD_H
