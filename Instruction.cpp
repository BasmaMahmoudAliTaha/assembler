#include "Instruction.h"
#include<bits/stdc++.h>

Instruction::Instruction(bool simple, std::string record, std::vector<std::string> operand,
                         std::string objcode, bool num, std::string mnemonic, int locctr, bool litteral,
                         std::string label, bool isOperation, bool indirect, bool indexed, bool immediate, int format,
                         bool externalRef, bool expression, std::vector<std::string> errorVector,
                         std::string comment, bool absolute, bool star) {
  this -> simple = simple;
  this -> record = record;
  this -> operand = operand;
  this -> objcode = objcode;
  this -> num = num;
  this -> mnemonic = mnemonic;
  this -> locctr = locctr;
  this -> litteral = litteral;
  this -> label = label;
  this -> isOperation = isOperation;
  this -> indirect = indirect;
  this -> indexed = indexed;
  this -> immediate = immediate;
  this -> format = format;
  this -> externalRef = externalRef;
  this -> expression = expression;
  this -> errorVector = errorVector;
  this -> comment = comment;
  this -> absolute = absolute;
  this -> star = star;
}

bool Instruction::isAbsolute(){
  return absolute;
}

void Instruction::setAbsolute(bool absolute) {
  this -> absolute = absolute;
}

std::string Instruction::getComment(){
  return comment;
}

void Instruction::setComment(std::string comment) {
  this -> comment = comment;
}

std::vector<std::string> Instruction::getErrorVector() {
  return errorVector;
}

void Instruction::setErrorVector(std::vector<std::string> errorVector) {
  this -> errorVector = errorVector;
}

bool Instruction::isExpression(){
  return expression;
}

void Instruction::setExpression(bool expression) {
  this -> expression = expression;
}

bool Instruction::isExternalRef(){
  return externalRef;
}

void Instruction::setExternalRef(bool externalRef) {
  this -> externalRef = externalRef;
}

int Instruction::getFormat() {
  return format;
}

void Instruction::setFormat(int format) {
  this -> format = format;
}

bool Instruction::isImmediate() {
  return immediate;
}

void Instruction::setImmediate(bool immediate) {
  this -> immediate = immediate;
}

bool Instruction::isIndexed() {
  return indexed;
}

void Instruction::setIndexed(bool indexed) {
  this -> indexed = indexed;
}

bool Instruction::isIndirect() {
  return indirect;
}

void Instruction::setIndirect(bool indirect) {
  this -> indirect = indirect;
}

bool Instruction::isIsOperation() {
  return isOperation;
}

void Instruction::setIsOperation(bool isOperation) {
  this -> isOperation = isOperation;
}

std::string Instruction::getLabel() {
  return label;
}

void Instruction::setLabel(std::string label) {
  this -> label = label;
}

bool Instruction::isLitteral() {
  return litteral;
}

void Instruction::setLitteral(bool litteral) {
  litteral = litteral;
}

int Instruction::getLocctr() {
  return locctr;
}

void Instruction::setLocctr(int locctr) {
  this -> locctr = locctr;
}

std::string Instruction::getMnemonic() {
  return mnemonic;
}

void Instruction::setMnemonic(std::string mnemonic) {
  this -> mnemonic = mnemonic;
}

bool Instruction::isNum() {
  return num;
}

void Instruction::setNum(bool num) {
  this -> num = num;
}

std::string Instruction::getObjcode() {
  return objcode;
}

void Instruction::setObjcode(std::string objcode) {
  this -> objcode = objcode;
}

std::vector<std::string> Instruction::getOperand() {
  return operand;
}

bool Instruction::isStar() {
  return  star;
}

void Instruction::setOperand(std::vector<std::string> operand) {
  this -> operand = operand;
}

std::string Instruction::getRecord() {
  return record;
}

void Instruction::setRecord(std::string record) {
  this -> record = record;
}

bool Instruction::isSimple() {
  return simple;
}

void Instruction::setSimple(bool simple) {
  this -> simple = simple;
}
std::vector<std::pair<std::string, std::string>> Instruction :: getExternalReference(){
  return extRefs ;
}
void Instruction::setIsNum (bool isNum){
  num = isNum ;
}