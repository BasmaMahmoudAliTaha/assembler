#include "GenerateRecord.h"


GenerateRecord::GenerateRecord(std::pair<std::string, int> start, int end, int lastLocCtr,
                               std::vector<std::string> extRefs,
                               std::vector<std::pair<std::string, std::string>> extDefs,
                               std::vector<Instruction> instructions) {
    this->extRefs = extRefs;
    this->extDefs = extDefs;
    this->instructions = instructions;
    this->start = start;
    this->end = end;
    this->lastLocCtr = lastLocCtr;
}

void  GenerateRecord::process() {

    freopen("record.txt", "w+", stdout);
    std::cout << "Instruction size: " << instructions.size() << std::endl;
    std::cout << GenerateRecord::createHeader() << std::endl;
    if (extDefs.size() != 0) {
        std::cout << GenerateRecord::createDefs() << std::endl;
    }
    if (extRefs.size() != 0) {
        std::cout << GenerateRecord::createRefs() << std::endl;
    }
    if (instructions.size() != 0) {
        std::vector<std::string> strs = GenerateRecord::createTexts();
        for (std::string str : strs) {
            std::cout << str << std::endl;
        }
    }
    if (modificationRecords.size() != 0) {
        for (std::string str : modificationRecords) {
            std::cout << str << std::endl;
        }
    }
    std::cout << GenerateRecord::createEnd() << std::endl;
}

std::string GenerateRecord::createDefs() {
    std::string defsString = "D";
    for (std::pair<std::string, std::string> s: extDefs) {
        defsString = defsString + s.first + s.second;
    }
    return defsString;
}

std::string GenerateRecord::createRefs() {
    std::string refsString = "R";
    for (std::string s: extRefs) {
        refsString = refsString + s;
    }
    return refsString;
}

std::vector<std::string> GenerateRecord::createTexts() {
    std::vector<std::string> strings;
    bool end = false;
    int instructionNum = 0;
    while (!end) {
        if (instructions.size() == 0) {
            return strings;
        }
        std::string str = "T" + GenerateRecord::validAddress(6, GenerateRecord::decimalToHex(
                GenerateRecord::instructions[instructionNum].getLocctr()));
        std::string str2 = "";
        while ((str2.length() + GenerateRecord::instructions[instructionNum].getObjcode().length()) <= 60) {
            if ((instructions[instructionNum].getMnemonic() == "RESW") ||
                (instructions[instructionNum].getMnemonic() == "RESB") ||
                    (instructions[instructionNum].getMnemonic() == "ORG")) {
                instructionNum++;
                if (instructionNum == GenerateRecord::instructions.size()) {
                    end = true;
                }
                break;
            }
            // modification record
            if (GenerateRecord::instructions[instructionNum].isAbsolute() ||
                GenerateRecord::instructions[instructionNum].getFormat() == 4) {
                GenerateRecord::createModRecs(GenerateRecord::instructions[instructionNum]);
            }
            str2 += GenerateRecord::instructions[instructionNum++].getObjcode();
            if (instructionNum == GenerateRecord::instructions.size()) {
                end = true;
                break;
            }


        }
        if (str2.size() != 0) {
            str += GenerateRecord::validAddress(2, GenerateRecord::decimalToHex(str2.length() / 2)) + str2;
            strings.push_back(str);
        }
    }
    return strings;
}

void GenerateRecord::createModRecs(Instruction instruction) {
    std::string str = "M";
    if (instruction.getMnemonic() == "WORD") {
        str += GenerateRecord::validAddress(6, GenerateRecord::decimalToHex(instruction.getLocctr())) + "06";
    } else {
        str += GenerateRecord::validAddress(6, GenerateRecord::decimalToHex(instruction.getLocctr() + 1)) + "05";
    }
    if (instruction.isExternalRef()) {
        std::vector<std::pair<std::string, std::string>> extRefs = instruction.getExternalReference();
        for (std::pair<std::string, std::string> p : extRefs) {
            GenerateRecord::modificationRecords.push_back(str + p.second + p.first);
        }
    } else {
        GenerateRecord::modificationRecords.push_back(str);
    }
}

std::string GenerateRecord::createHeader() {
    std::string header = "H" + GenerateRecord::start.first;
    for (int i = 0; i < 6 - GenerateRecord::start.first.length(); i++) {
        header += " ";
    }
    std::string temp = GenerateRecord::decimalToHex(GenerateRecord::start.second);
    temp = GenerateRecord::validAddress(6, temp);
    header += temp;
    int temp2 = GenerateRecord::lastLocCtr - GenerateRecord::start.second;
    temp = GenerateRecord::decimalToHex(temp2);
    temp = GenerateRecord::validAddress(6, temp);
    header += temp;
    return header;

}

std::string GenerateRecord::createEnd() {
    return ("E" + GenerateRecord::validAddress(6, GenerateRecord::decimalToHex(GenerateRecord::end)));
}

std::string GenerateRecord::decimalToHex(int x) {
    std::stringstream stream;
    stream << std::hex << x;
    std::string result(stream.str());
    for (int i = 0; i < result.size(); i++) {
        if (result[i] >= 'a' && result[i] <= 'z') {
            result[i] = result[i] - 32;
        }
    }
    return result;
}

std::string GenerateRecord::validAddress(int wantedSize, std::string str) {
    std::string valid;
    int numOfZeros = wantedSize - str.size();
    for (int i = 0; i < numOfZeros; i++) {
        valid += "0";

    }
    valid += str;
    return valid;
}