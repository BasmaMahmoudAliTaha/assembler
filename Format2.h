//
// Created by Microsoft on 5/6/2016.
//

#ifndef PASS1_FORMAT2_H
#define PASS1_FORMAT2_H

#include "Format.h"

class Format2 : public Format {
public:
    Format2 (std::map <std::string, std::string> opcode ,std::map<std::string , int> symbolTable,
             std::vector<std::string> externalReference,   std::map <std::string, int > registers , std::map <std::string , bool > symTabAbs)
            : Format(opcode , symbolTable , externalReference , registers, symTabAbs){

    }

    std::string formFirstTwoBits(Instruction &instruction);
    std::string formAddressBits(Instruction &instruction , bool isBase, int baseValue);
    std::string formThirdBit(Instruction &instruction);
    std::string process(Instruction &instruction, bool &isBase, int &baseVal);

};


#endif //PASS1_FORMAT2_H
