#include "Parser.h"
#include "Instruction.h"
#include "Parser.h"
#include "Checker.h"
#include <iostream>
#include <bits/stdc++.h>
#include <vector>
#include <string>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <tuple>
#include <stdexcept>
#include <boost/variant.hpp>
#include <boost/algorithm/string.hpp>
Parser::Parser(){
  recordIndex = 0;
  fileSize = 0;
  tokensNumber = 0;
}

bool Parser::isBlank(std::string s){
  if(s.length() == 0)
    return true;
  for(int i=0 ; i<s.length() ; i++){
    if(s.at(i) >= 'A' && s.at(i) <= 'Z' || s.at(i) >= '0' && s.at(i) <= '9' || s.at(i) == ',' || s.at(i) == '*' || s.at(i) == '.' || s.at(i) >= 'a' && s.at(i) <= 'z')
      return false;
  }
  return true;
}

std::string removeSpaces(std::string str){
  std::regex byteChar("=?\\s*[Cc]\\s*(\\'.*\\')\\s*");
  std::smatch match;
  if(std::regex_match(str, match,byteChar)){
    std::string temp1 = match.str(1);
//    boost::erase_all(str," ");
//    boost::erase_all(str,"\t");
    std::string temp = "";
    if(str.at(0) == '=')
      temp += '=';
    temp = temp+ 'C' + temp1 ;
    return temp;
  }
  std::transform(str.begin() , str.end() , str.begin() , toupper);
  boost::erase_all(str," ");
  boost::erase_all(str,"\t");
  return str;
}

void Parser::prepareInstruction(std::string fullRecord){
  label = "";
  mnemonic = "";
  operand.clear();
  errorVector.clear();
  isOperation = true;
  comment = "";
  indexed = false;
  format = 0;
  immediate = false;
  indirect = false;
  num = false;
  expression = false;
  litteral = false;
  externalRef = false;
  simple = false;
  absolute = false;
  star = false;

  std::regex commentExp("[\t\n\\s]*(\\..+)[\t\n\\s]*");
  //std::regex statement("\\s*(\\w+\\s+)?(\\+?\\w+)\\s+((\\w+\\s*,\\s*\\w+)|([#@\\w\\*\\']+\\s*))\\s*");
  //std::regex statement("\\s*(\\w+\\s+)?(\\+?\\w+)\\s+((\\w+\\s*,\\s*\\w+)|([#@XC\\*]?\\s*\\'?\\s*\\w*\\s*\\'?))\\s*");    // ta3dil
  std::regex statement("\\s*(\\w+\\s+)?(\\+?\\w+)\\s+((\\w+\\s*,\\s*\\w+)|([#@\\*]?\\s*\\w*)|(=?\\s*[Cc]\\s*\\'.+\\')|(=?\\s*[Xx]\\s*\\'\\w+\\')|(#?\\s*\\-\\d+)|(\\s*[+-]?\\s*(\\s*\\w+\\s*[+-/*]\\s*)+\\w+)|(\\s*=\\s*\\*))\\s*");
  std::regex oneOperation("\\s*(\\w+\\s+)?(\\+?\\w+)\\s*");
  Checker checker(opCodeNumeric, registers, directives);
  std::vector<std::string> tokens;
  std::smatch match1, match2, match3;
  checker.resetFlags();

  if(std::regex_match(fullRecord, match1,commentExp)&& match1.size() > 1){
    comment = match1.str(1);
    return;

  }
  else if(std::regex_match(fullRecord, match2,statement)&& match2.size() > 1) {

    if(match2.str(1) == ""){
      tokensNumber = 2;
      std::string mnemonic = removeSpaces(match2.str(2));
      tokens.push_back(mnemonic);
      std::string concat;
      concat = removeSpaces( match2.str(3));
      tokens.push_back(concat);
    }else{
      std::string label = removeSpaces(match2.str(1));
      if(checker.isKeyWord(label)){
        std::string operand = "";
        for(int i = 2 ; i < match2.size() - 1; i++){
          std::string temp = removeSpaces(match2.str(i));
          operand += temp;
        }
        tokensNumber = 2;
        tokens.push_back(label);
        tokens.push_back(operand);
      }else {
        std::string mnemonic = removeSpaces(match2.str(2));
        tokensNumber = 3;
        tokens.push_back(label);
        tokens.push_back(mnemonic);
        std::string concat;
        concat = removeSpaces(match2.str(3));
        tokens.push_back(concat);
      }
    }

  } else if(std::regex_match(fullRecord, match3,oneOperation) && match3.size() > 1){

    if(match3.str(1) == ""){ // no label
      tokensNumber = 1;
      std::string tmp = removeSpaces(match3.str(2));
      tokens.push_back(tmp);
    }else{ //exist label
      tokensNumber = 2;
      std::string label = removeSpaces(match3.str(1));
      std::string tmp = removeSpaces(match3.str(2));
      tokens.push_back(label);
      tokens.push_back(tmp);
    }

  }
  else{
    errorVector.push_back("INVALID SYNTAX");
    return;
  }

  if (tokensNumber == 1) {
    std::string temp = tokens[0];   //t3dil
    if (tokens[0].at(0) == '+') {
      format = 4;
      tokens[0] = tokens[0].substr(1, tokens[0].size());
    }

    if (tokens[0].compare("RSUB") == 0 ||tokens[0].compare("END") == 0  ) {
      label = "";
      mnemonic = tokens[0];
      std::string tempString = "";
      operand.push_back(tempString);
      isOperation = true;
      format = 3;
      if(tokens[0].compare("END") == 0){
        format = 5;
        isOperation = false;
      }

    } else if (temp[0] != '+' && checker.isDirective(tokens[0])) {  // t3dil
      isOperation = false;
      mnemonic = tokens[0];
      return;
    } else
      errorVector.push_back("SYNTAX ERROR");
  } else if (tokensNumber == 2) {
    if (tokens[0] == "+RSUB") {
      tokens[0] = tokens[0].substr(1, tokens[0].size());
    }else if(tokens[1] == "+RSUB"){
      format = 4;
      tokens[1] = tokens[1].substr(1, tokens[1].size());
    }
    if (tokens[1].compare("RSUB") == 0) {
      bool validSyntax = checker.isValidSyntax(tokens[0]);
      bool isKeyword = checker.isKeyWord(tokens[0]);
      if (isKeyword || !validSyntax) {
        label = tokens[0];
        //  labels.push_back(tokens[0]);
        mnemonic = tokens[1];
        errorVector.push_back("INVALID LABEL");
        return;
      }
      else if (!isKeyword && validSyntax) {
        if(format != 4)
          format = 3;
        label = tokens[0];
        mnemonic = tokens[1];
      }
    } else if (tokens[0].compare("RSUB") == 0){
      errorVector.push_back("INVALID OPERATION");
    } else{
      if (checker.isKeyWord(tokens[0]) && registers.find(tokens[0]) == registers.end()) {
        checker.determineOperandType(tokens[1]);
        format = checker.validOperand(tokens[0], tokens[1]);
        if (format != 0 ) {
          if (checker.isDirective(tokens[0]))
            isOperation = false;
          mnemonic = tokens[0];
          operand = checker.getOperand();
          indexed = checker.isIndexed();
          immediate = checker.isImmediate();
          indirect = checker.isIndirect();
        } else {
          errorVector.push_back("INVALID OPERAND");
        }
      } else {
        errorVector.push_back("INVALID OPERATION");
      }
    }
    //mnemonic = tokens[0];
    operand = checker.getOperand();
    indexed = checker.isIndexed();
    immediate = checker.isImmediate();
    indirect = checker.isIndirect();
    num = checker.isNum();
    litteral = checker.isLitteral();
    externalRef = checker.isExternalRef();
    expression = checker.isExpression();
    simple = checker.isSimple();
    star = checker.isStar();
    absolute = checker.isAbsolute();
    return;
  }

  if (tokensNumber == 3) {
    if(tokens[1].compare("RSUB") == 0 ||tokens[1].compare("+RSUB") == 0  ){
      errorVector.push_back("INVALID OPERATION");
      return;
    }
    if (!checker.isKeyWord(tokens[0]) && checker.isValidSyntax(tokens[0])) {
      label = tokens[0];
      labels.push_back(tokens[0]);
    }
    else {
      errorVector.push_back("INVALID LABEL");
    }
    format = checker.validOperand(tokens[1], tokens[2]);

    if (checker.isKeyWord(tokens[1]) && registers.find(tokens[1]) == registers.end() && format != 0) {  // t3dil
      checker.determineOperandType(tokens[2]);
      if (checker.isDirective(tokens[1]))
        isOperation = false;
      indexed = checker.isIndexed();
      immediate = checker.isImmediate();
      indirect = checker.isIndirect();
      num = checker.isNum();
      litteral = checker.isLitteral();
      externalRef = checker.isExternalRef();
      expression = checker.isExpression();
      simple = checker.isSimple();
      absolute = checker.isAbsolute();
      star = checker.isStar();
    }
    else {
      errorVector.push_back("INVALID OPERATION");
    }
    label = tokens[0];
    mnemonic = tokens[1];
    operand = checker.getOperand();
    return;
  }
}

std::vector<std::string> Parser::readFile(std::string file){
  std::vector <std::string> fullRecords;
  std::string line;
  std::ifstream myfile (file);
  if (myfile.is_open()){
    while ( getline (myfile,line) ){
      int stringSize = line.size();
      std::string temp = line;
      //  std::transform(line.begin() , line.end() , line.begin() , toupper);
      if(line.size() > 1 && line.at(line.size()-1) == '\r' ){
        line = line.substr(0, line.size()-1);
      }
      if(!isBlank(line)) {
        fileSize++;
        instructionsAsItIs.push_back(temp);
        recordBuffer.push_back((line));
      }
    }
    myfile.close();
  }

  return fullRecords;
}


int Parser::getFileSize(){
  return fileSize;
}


std::map<std::string, std::vector < std::string> > Parser::getFullAppendix(){
  return fullAppendix;
}


void Parser::parseAppendix(std::string file){

  using namespace boost::algorithm;
  std::vector <std::string> fullRecords;

  std::string line;
  std::ifstream myfile ("myfile.txt");

  if (myfile.is_open()){
    int i= 0;
    while ( getline (myfile,line) ){
      i++;
      fullRecords.push_back((line));
    }
    myfile.close();
  }

  for(int i=0 ; i<fullRecords.size() ; i++ ){

    std::string str = fullRecords[i];
    std::vector<std::string> tokens;
    std::vector<std::string> filteredTokens;

    split(tokens, str, is_any_of(" \t \n"));

    for(int j=0;j<tokens.size();j++){

      if( tokens[j].length() == 0 ) {
        tokens.erase(tokens.begin() + j);
        j = 0;
      }
    }

    std::vector < std::string > temp;
    int tokensLength = tokens.size();

    for(int j=1 ; j<tokensLength ; j++)
      temp.push_back(tokens[j]);


    opCodeNumeric[tokens[0]] = atoi(tokens[tokensLength - 1].c_str());
    fullAppendix[tokens[0]] = temp;
    opCode[tokens[0]] = tokens[tokensLength - 1];
  }
}

void Parser::loadAppendix(){
  std::string fileName = "myfile";
  parseAppendix(fileName);
  registers["A"] = 0;
  registers["X"] = 1;
  registers["L"] = 2;
  registers["B"] = 3;
  registers["S"] = 4;
  registers["T"] = 5;
  registers["F"] = 6;
  registers["PC"] = 8;
  registers["SW"] = 9;
  directives["START"] = 0;
  directives["EQU"] = 1;
  directives["BYTE"] = 2;
  directives["RESB"] = 3;
  directives["WORD"] = 4;
  directives["RESW"] = 5;
  directives["ORG"] = 6;
  directives["END"] = 7;
  directives["BASE"] = 8;
  directives["NOBASE"] = 9;
  directives["LTORG"] = 10;
}

Instruction Parser::getInstruction() {
  prepareInstruction(recordBuffer[recordIndex++]);
  Instruction instruction(simple, instructionsAsItIs[recordIndex-1], operand, "", num, mnemonic, 0, litteral, label, isOperation,
                          indirect, indexed, immediate, format, externalRef, expression, errorVector, comment, format == 4 ? true : absolute,star );
//  std::cout << "*************************************************************" << std::endl;
//  std::cout<< instructionsAsItIs[recordIndex-1]<<std::endl;
//  std::cout<< "Operand"<<std::endl;
//  for(int i = 0; i < operand.size(); ++i)
//    std::cout<<operand.at(i)<<std::endl;
//  std::cout<< "label "<<label <<std::endl;
//  std::cout<< "Mnemonic "<<mnemonic<<std::endl;
//  std::cout<< "Format "<<format<<std::endl;
//  std::cout<< "expres "<<expression<<std::endl;
//  std::cout<<"Num " <<num<<std::endl;
//  std::cout<< "litteral "<<litteral<<std::endl;
//  std::cout<< "Simple "<<simple<<std::endl;
//  std::cout<< "Indirict  "<<indirect<<std::endl;
//  std::cout<< "immediate "<<immediate<<std::endl;
//  std::cout<< "indexed "<<indexed<<std::endl;
//  std::cout << "*************************************************************" << std::endl;

  return instruction;
}

std::vector<std::string> getLabels(){

}

std::map<std::string, std::string> Parser::getOpCode(){
  return opCode;
}

std::map <std::string, int > Parser::getRegisters(){
  return registers;
}

std::vector<std::string> Parser::splitAscii(std::string str,std::regex rgx){
  std::sregex_token_iterator iter(str.begin(),str.end(),rgx,-1);
  std::sregex_token_iterator end;
  std::vector<std::string> ans;
  int i = 0;
  for ( ; iter != end; ++iter){
    ans.push_back(*iter);
    ++i;
  }
  if(ans.at(0).compare("") == 0){
    ans.erase(ans.begin());
  }
  return ans;
}


void Parser::parseAscii(std::string fileName){
  std::vector <std::string> fullRecords;
  std::string line;
  std::ifstream asciiFile (fileName);
  if (asciiFile.is_open()){
    int i = 0;
    while ( getline (asciiFile,line) ){
      fullRecords.push_back((line));
      //std::cout << fullRecords.at(i) << std::endl;
      i++;
    }
    asciiFile.close();
  }
  for(int i = 1 ; i < fullRecords.size() ; ++i){
    std::regex spaces("\\s+");
    std::vector < std::string > temp = splitAscii(fullRecords.at(i), spaces);
    std::string key = temp.at(2);
    temp.erase(temp.begin() + 2);
    asciiMap.insert(std::pair<std::string, std::vector < std::string >>(key,temp));
  }
}

std::map <std::string, std::vector < std::string > > Parser::getAsciiMap(){
  return asciiMap;
}