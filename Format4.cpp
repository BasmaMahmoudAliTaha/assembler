#include "Format4.h"

std::string Format4::process(Instruction &instruction, bool &isBase, int &baseVal) {
    if(instruction.getMnemonic() == "RSUB"){
        return "4F000000";
    }
    std::string first, second, third;
    first = Format::formFirstTwoBits(instruction);
    third = Format4::formAddressBits(instruction, isBase, baseVal);
    second = Format4::formThirdBit(instruction);
    //std::cout << "first "<<first <<  second << third << std::endl;
    return first + second + third;
}

std::string Format4::formThirdBit(Instruction &instruction) {
    std::string str;
    if (instruction.isStar())
        str = "0001";
    else if (instruction.isIndexed())
        str = "1001";
    else
        str = "0001";
    return Format::fromBinaryStringToHexaString(str);
}

std::string Format4::formAddressBits(Instruction &instruction, bool isBase, int baseVal) {
    std::string address;
    if(symTabAbs.find(instruction.getOperand()[0]) != symTabAbs.end() ){
        int temp  = symbolTable[instruction.getOperand()[0]];
        if(temp < 0 && !instruction.isImmediate()  ) {
            Format::printError("Negative value in address", instruction);
            return "00000";
        }
        else
            instruction.setAbsolute(true);
    }
    if(instruction.isStar()){
        int temp = instruction.getLocctr() + 4;
        address = Format::fromIntToHex(temp);

    }

    else if ((!instruction.isNum() || instruction.isExpression()) && !instruction.isAbsolute()) {
        int temp = atoi(instruction.getOperand()[0].c_str());
        address = Format::fromIntToHex(temp);
        std::map<std::string, int>::iterator itr;
        itr = symbolTable.find(instruction.getOperand()[0]);
        if (itr != symbolTable.end()) {// find label in symbol table
            temp = itr->second;
            address = Format::fromIntToHex(temp);
        } else {
            Format::printError("Label not found in symbolTable", instruction);
        }

    }
    else if ((instruction.isImmediate() || instruction.isIndirect()) && instruction.isNum()) {
        std::string temp = instruction.getOperand()[0];
//        std::cout <<"temp: "<< temp << std::endl;
        address = Format::fromIntToHex(Format::fromStringToInt(temp));
    }
    else if (instruction.isAbsolute()) {

            if (instruction.isExpression()) {
                if(Format::fromStringToInt(instruction.getOperand()[0])<0 && !instruction.isImmediate()){
                    Format::printError("Invalid Address",instruction);
                    return "00000";
                }
                std::string temp = instruction.getOperand()[0];
                address = Format::fromIntToHex(Format::fromStringToInt(temp));
                if (!(address.length() <= 5)) {
                    Format::printError("Address cant fit in format 4 ", instruction);
                    return "00000";
                }


        } else {
            std::string temp = instruction.getOperand()[0];
            int temp2 ;
            if(!instruction.isNum()){
                temp2 = symbolTable[temp];
                address = Format::fromIntToHex(temp2) ;
                if(address.size() > 5) {
                    Format::printError("Address cant fit in format 4 ", instruction);
                    return "00000";
                }
                return Format::validAddress(5, address);
            }
            if (temp.length() <= 5)
                address = temp;
            else
                Format::printError("Address cant fit in format 4 ", instruction);
        }
    }
//    std::cout <<"address "<<Format::validAddress(5, address)<< std::endl;
    return Format::validAddress(5, address);
}