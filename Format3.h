//
// Created by Microsoft on 5/3/2016.
//

#ifndef PASS1_FORMAT3_H
#define PASS1_FORMAT3_H
#include "Format.h"
#include "Instruction.h"

class Format3 : public Format  {
private:
    int b = 0;
    int p = 0;

public:
    Format3 (std::map <std::string, std::string> opcode ,std::map<std::string , int> symbolTable,
             std::vector<std::string> externalReference,   std::map <std::string, int > registers, std::map <std::string, bool> symTabAbs)
            : Format(opcode , symbolTable , externalReference , registers, symTabAbs){

    }

    std::string formAddressBits(Instruction &instruction , bool isBase, int baseValue);
    std::string formThirdBit(Instruction &instruction);
    std::string process(Instruction &instruction, bool &isBase, int &baseVal);
};


#endif //PASS1_FORMAT3_H
