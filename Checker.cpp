#include "Checker.h"
#include <bits/stdc++.h>
#include <regex>
Checker::Checker(std::map <std::string, int> opCode, std::map <std::string, int> registers,std::map <std::string, int> dirictives){
  this->opCode = opCode;
  this->registers = registers;
  this->directives = dirictives;
}

void Checker::resetFlags(){
  indexed = false;
  immediate = false;
  indirect = false;
  litteral = false;
  num = false;
  externalRef = false;
  expression = false;
  simpleVal = false;
  absolute = false;
  star = false;
}

bool Checker::isKeyWord(std::string field) {
  bool flag = false;
  std::string temp = "";
  if (field.at(0) == '+') {
    for (int i = 1; i < field.length(); i++) {
      temp += field.at(i);
    }
    flag = true;
  }
  if (flag) {
    if(field.at(field.size()-1)=='R' || temp.compare("RMO") == 0 || temp.compare("SHIFTL") == 0 )   // t3dil
      return false;
    if(opCode.find(temp) != opCode.end())
      return true;
    else
      return false;
  }
  else {
    if (opCode.find(field) != opCode.end() || registers.find(field) != registers.end() || directives.find(field) != directives.end() ) {
      return true;
    }
    else
      return false;
  }
}

bool Checker::isValidSyntax(std::string label){
  std::regex labelPattern("(_?[A-Za-z]+(\\w*))");
  if(regex_match(label,labelPattern)){
    return true;
  }
  return false;
}

bool Checker::isValidAddress(std::string operand){
  std::regex decimal("(\\d+)");
  std::regex hex("[0-9a-fA-F]+");
  if(std::regex_match(operand, decimal) || std::regex_match(operand, hex)){
    return true;
  }
  return false;
}

void Checker::determineOperandType(std::string operand) {
  std::regex simple("\\-?(\\w+)");
  std::regex lit("((=C\\'.+\\')|(=X\\'\\w+\\'))");
  std::regex express("(\\s*[+-]?\\s*(\\s*\\w+\\s*[*+-/]\\s*)+\\s*\\w+)");
  std::smatch match;
  std::string label;
  if (operand[0] == '=')
    litteral = true;
  if (std::regex_match(operand, simple) && std::regex_search(operand, match, simple) && match.size() > 1) {
    label = match.str(1);
    if ((isValidSyntax(label) || isValidAddress(label)) && !isKeyWord(label)) {
      Checker::simpleVal = true;
      if (isHex(label)) {
        num = true;
        absolute = true;
        return;
      }
    }
  } else if (std::regex_match(operand, match, lit) && match.size() > 1) {
      Checker::litteral = true;
      return;
    }else if(std::regex_match(operand, match ,express) &&  match.size() > 1 ){
      if(!findCharInString(operand , ',')) {
       Checker::expression = true;
      //operands.push_back(match.str(1));
      }
      return ;
    } else {
      if (operand.compare("*") == 0) {
        star = true;
        return;
      } else if (operand.compare("=*") == 0) {
        litteral = true;
        return ;
    }
  }
}

bool Checker::findCharInString(std::string str , char c){
  for(int i = 0 ; i < str.size() ; i++){
    if(str[i] == c)
      return true;
  }
  return false;
}

int Checker::validOperand(std::string mnemonic , std::string operand){
  int format = 0;
  int mnemonicLength = mnemonic.size();
  if(isDirective(mnemonic)){
    format = 5;
    if(mnemonic.compare("BYTE") == 0){
      if(operand.at(0) == 'X'){
        std::string tmp = operand.substr(2,operand.size()-3);
        if(isValidAddress(tmp)){
          //check for this
          operands.push_back(operand);
          return format;
        }
        else
          return 0;
      }
    }
    operands.push_back(operand);
    return format;
  }else{
    if ( mnemonic.compare("TIXR") == 0 || mnemonic.compare("CLEAR") == 0 ){
      if(isValidRegister(operand)) {
        format = 2;
        operands.push_back(operand);
      }
    }else if(mnemonic.compare("SHIFTR") == 0 || mnemonic.compare("SHIFTL") == 0 ){
      std::regex shift("(\\w+),(\\d+)");
      std::smatch match;
      if(std::regex_match(operand, shift)&&std::regex_search(operand,match, shift) && match.size() > 1){
        std::string reg = match.str(1);
        std::string dec = match.str(2);
        std::string::size_type sz;
        if(isValidRegister(reg) && std::stoi (dec,&sz) <= 8 ){
          format = 2;
          operands.push_back(reg);
          operands.push_back(match.str(2));
        }
      }
    }
    else if((mnemonic.at(mnemonicLength-1) == 'R' || mnemonic.compare("RMO") == 0)){
      if(isValidForFormatTwo(mnemonic,operand)) {
        format = 2;
      }
    }
    else{ // format 3 or 4
      if(mnemonic.at(0) == '+' && isValidForFormat3And4(mnemonic.substr(1),operand)){  // substr
        format = 4;
      }
      else if(mnemonic.at(0) != '+' && isValidForFormat3And4(mnemonic,operand)){
        format = 3;
      }
    }
    return format;
  }
}

bool Checker::isIndexed(){
  return indexed;
}

bool Checker::isImmediate(){
  return immediate;
}

bool Checker::isIndirect() {
  return indirect;
}

bool Checker::isExpression() {
  return expression;
}

bool Checker::isExternalRef() {
  return externalRef;
}

bool Checker::isLitteral() {
  return litteral;
}

bool Checker::isNum() {
  return num;
}

bool Checker::isSimple(){
  return simpleVal;
}

bool Checker::isValidForFormat3And4(std::string mnemonic ,std::string operand){
  std::regex immediate1("(@|#)(\\-?\\d+)");
  std::regex immediateOrIndirect("(#|@)(\\w+)");
  std::regex indexed("(\\w+),(X)");
  std::regex simple("(\\-?\\w+)");
  std::regex lit("((=C\\'.+\\')|(=X\\'\\w+\\'))");
  std::regex express("(\\s*[+-]?\\s*(\\s*\\w+\\s*[+-/*]\\s*)+\\w+)");
  std::smatch match;
  std::string label;
  try{
    if(std::regex_match(operand, immediate1)&&std::regex_search(operand, match, immediate1) && match.size() > 1){
      if(match.str(1) == "#")
        immediate = true;
      else {
        indirect = true;
        if(match.str(2)[0] == '-')
          return false;
      }
      num = true;
      operands.push_back(match.str(2));
      return true;
    }
    else if(std::regex_match(operand, immediateOrIndirect)&&std::regex_search(operand, match, immediateOrIndirect) && match.size() > 1){
      if(match.str(1).compare("#") == 0){
        immediate = true;
      }else{
        indirect = true;
      }
      label = match.str(2);
      operands.push_back(match.str(2));
      if(immediate)
        return (isValidSyntax(label) && !isKeyWord(label) && !isValidAddress(label));
      else if(indirect){
        return  (isValidSyntax(label)||isValidAddress(label)) && !isKeyWord(label);
      }
    }
    else if(std::regex_match(operand, indexed)&&std::regex_search(operand, match, indexed) && match.size() > 1){
      label = match.str(1);
      this->indexed = true;
      simpleVal = true;
      operands.push_back(label);
      operands.push_back(match.str(2));
      return (isValidSyntax(label) && !isKeyWord(label) && !isValidAddress(label));
    }else if (std::regex_match(operand, simple)&&std::regex_search(operand, match, simple) && match.size() > 1) {
      label = match.str(1);
      Checker::simpleVal = true;
      if(isHex(label)) {
        num = true;
        absolute = true;
      }
      operands.push_back(label);
      return ((isValidSyntax(label) || isValidAddress(label)) && !isKeyWord(label));
    }else if(std::regex_match(operand, match ,lit) &&  match.size() > 1 ){
      Checker::litteral = true;
      operands.push_back(match.str(1));
      return true;
    }else if(std::regex_match(operand, match ,express) &&  match.size() > 1 ){
     if(!findCharInString(operand,',')){
      Checker::expression = true;
      operands.push_back(match.str(1));
      return true;
     }
    }
    else {
      if (operand.compare("*") == 0) {
        operands.push_back("*");
        star = true;
        return true;
      } else if (operand.compare("=*") == 0) {
        operands.push_back("=*");
        litteral = true;
        return true;
      }
    }
    return false;
  }catch(std::regex_error e){
    std::cout << "regex syntax error"<<std::endl;
  }
}


bool Checker::isHex(std::string operand)
{
  std::regex hex("\\-?[0-9a-fA-F]+");
  if(std::regex_match(operand, hex))
    return true;
  return false;
}

bool Checker::isAbsolute() {
  return absolute;
}

bool Checker::isStar() {
  return star;
}

bool Checker::isValidForFormatTwo(std::string mnemonic, std::string operand){
  std::regex twoOperands("(\\w+),(\\w+)");
  std::smatch match;
  try{

    if(std::regex_match(operand, twoOperands)&&std::regex_search(operand, match, twoOperands) && match.size() > 1){
//      for(int i = 1; i < match.size(); i++){
//        std::cout<<match.str(i)<<std::endl;
//      }
      operands.push_back(match.str(1));
      operands.push_back(match.str(2));
      return (isValidRegister(match.str(1))&& isValidRegister(match.str(2)));
    }
    else{
      return false;
    }
  }catch(std::regex_error e){
    std::cout << "regex syntax error"<<std::endl;
  }
}

bool Checker::isValidRegister(std::string operand){
  if(operand.compare("SW") == 0 || operand.compare("PC") == 0 ){
    return false;
  }
  if ( registers.find(operand) == registers.end() ) {
    return false;
  }
  return true;
}

bool Checker::isDirective(std::string operand){
  if(directives.find(operand) == directives.end()){
    return false;
  }
  return true;
}

std::vector<std::string> Checker::getOperand(){
  return operands;
}