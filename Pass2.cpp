#include "Pass2.h"

Pass2::Pass2(std::map<std::string, std::string> opcode, std::vector<Instruction> instructions,
             std::map<std::string, int> symbolTable, std::map<std::string, int> registers,
             std::vector<std::string> externalReference, std::map<std::string, std::vector<std::string> > asciiMap,
             std::map<std::string, int> littab, std::map<std::string, bool> symTabAbs) {
    this->instructions = instructions;
    this->baseUsed = false;
    this->startFound = false;
    this->baseValue = -1;
    this->symbolTable = symbolTable;
    this->opcode = opcode;
    this->externalReference = externalReference;
    this->registers = registers;
    this->asciiMap = asciiMap;
   generalExpression.setSymTable(symbolTable);
    generalExpression.setSymTableRelOrAbs(symTabAbs);
    for (auto i = littab.begin(); i != littab.end(); i++) {
        this->symbolTable.insert(std::make_pair(i->first, i->second));
    }

    this->symTabAbs = symTabAbs;
}

int Pass2::stringToInt(std::string str) {
    std::string stringConverted = "";
    int integer = 0;
    for (int i = str.length() - 1; i >= 0; i--) {
        integer += (str.at(i) - '0') * power(10, i);
    }
    return integer;
}

int Pass2::power(int base, int power) {
    int value = 1;
    for (int i = 0; i < power; i++) {
        value *= base;
    }
    return value;
}

std::vector<Instruction> Pass2::processInstructions() {

    for (int i = 0; i < instructions.size(); i++) {
        dummyFound = false;
        objectCode = "";
        Instruction instruction = instructions[i];

        if (!startFound) {
            if (!instruction.isIsOperation() && instruction.getMnemonic().compare("START") == 0) {
                startFound = true;
                programName = instruction.getLabel();
                startAddress = std::stoi(instruction.getOperand()[0], nullptr, 16);
                std::cout << instruction.getLabel() << "\t" << "START" << "\t" << instruction.getOperand()[0] << "\t" <<
                std::endl;
            }
            else if (instruction.isIsOperation()) {
                errorVector.push_back("START not found yet");
            }
        }
        else if (startFound) {
            //format 3 // LDA  #200
//      std::vector<std::string> errorVector;
//      instruction.setErrorVector(errorVector);
//      instruction.setFormat(4);
//      instruction.setIndexed(false);
//      instruction.setIsNum(true);
//      std::vector<std::string> operand;
//      operand.push_back("200");
//
//      //instruction.setImmediate(true);
//      instruction.setIndirect(true);
//      instruction.setMnemonic("LDA");
//      instruction.setOperand(operand);

            //format 4   LDS   @1000
//      std::vector<std::string> errorVector;
//      instruction.setErrorVector(errorVector);
//      instruction.setFormat(3);
//      instruction.setIndexed(false);
//      instruction.setIsNum(true);
//      std::vector<std::string> operand;
//      operand.push_back("1000");
//      instruction.setIndirect(true);
//      instruction.setMnemonic("LDS");
//      instruction.setOperand(operand);
            //format 4   LDS   @1000
            //std::vector<std::string> errorVector;

            //FORMAT 3 ,4 with label  20  LDF   LABEL (1000)
//      instruction.setErrorVector(errorVector);
//      instruction.setFormat(4);
            //instruction.setImmediate(true) ;
            //instruction.setIndirect(true);
            //instruction.setIndexed(true);
//      instruction.setIsNum(false);
            //baseUsed = true ;
            //baseValue =  5000 ;
//      instruction.setLocctr(300);
//      symbolTable.insert(std::make_pair("LABEL",54));
//      std::vector<std::string> operand;
//      operand.push_back("LABEL");
//      std::cout<< "indirect: "<< instruction.isIndirect()<<std::endl ;
//      instruction.setMnemonic("JSUB");
//      instruction.setOperand(operand);
            std::vector<std::string> errorVector;
            if (instruction.getErrorVector().size() == 0) {
                if (instruction.getComment().length() > 0) {
                    print(instruction);
                    continue;
                }

                if (instruction.isIsOperation()) {
                    std::string operand = "";
                    if(instruction.getOperand().size() > 0)
                         operand = instruction.getOperand()[0];
                    if (instruction.getMnemonic().compare("LDB") == 0) {

                        if (!instruction.isNum()) {
                            baseValue = symbolTable[operand];
                        }
                        else {
                            int temp = std::stoi(operand, nullptr, 16);
                            baseValue = temp;
                        }
                    }



                    ///General Expression Part in Pass2
                    if (instruction.isExpression()) {
                        std::pair<bool, long long> parsedExp = generalExpression.getValue(instruction.getOperand()[0]);
                        //instruction.setAbsolute(false);
                        //parsedExp.first = true;
                        //parsedExp.second = symbolTable["CLOOP"];
                        if (!parsedExp.first) {
                            instruction.setAbsolute(generalExpression.isAbsoluteOrRel());
                            if(generalExpression.isAbsoluteOrRel()) {
                                std::vector<std::string> tempOperandVec;
                                std::string stringParsedExp = std::to_string(parsedExp.second);
                                tempOperandVec.push_back(stringParsedExp);
                                instruction.setOperand(tempOperandVec);
                                instruction.setIsNum(true);
                            }else{
                                bool foundInSymboleTable = false;
                                for(auto i = symbolTable.begin() ; i!=symbolTable.end() ; i++ ){
                                    if(parsedExp.second == i->second){
                                        std::vector<std::string> tempOperandVec;
                                        std::string stringParsedExp = i->first;
                                        tempOperandVec.push_back(stringParsedExp);
                                        instruction.setOperand(tempOperandVec);
                                        foundInSymboleTable = true;
                                        break ;
                                    }
                                }
                                if(!foundInSymboleTable){
                                    if(parsedExp.second < 0){
                                        std::vector<std::string> temp = instruction.getErrorVector();
                                        temp.push_back("Invalid Operand field");
                                        instruction.setErrorVector(temp);
                                        print(instruction);
                                        continue;
                                    }
                                    std::vector<std::string> tempOperandVec;
                                    std::string stringParsedExp = "dummy";
                                    tempOperandVec.push_back(stringParsedExp);
                                    instruction.setOperand(tempOperandVec);
                                    foundInSymboleTable = true;
                                    dummyFound = true;
                                    symbolTable.insert(std::make_pair("dummy",parsedExp.second));
                                }
                            }
                        }
                        else {
                            //errorVector.push_back("Invalid Expression");
                            std::vector<std::string> temp = instruction.getErrorVector();
                            temp.push_back("Invalid Expression");
                            instruction.setErrorVector(temp);
                            print(instruction);
                            continue;
                        }

                    }


                    if (instruction.getFormat() == 2) {
                        Format2 format2(opcode, symbolTable, externalReference, registers, symTabAbs);
                        objectCode = format2.process(instruction, baseUsed, baseValue);
                    }
                    if (instruction.getFormat() == 3) {
                       // std::cout << "entered 3 " << std::endl;
                        Format3 format3(opcode, symbolTable, externalReference, registers, symTabAbs);
                        objectCode = format3.process(instruction, baseUsed, baseValue);
                       // std::cout << "entered 4 " << std::endl;
                    }
                    if (instruction.getFormat() == 4) {
                        Format4 format4(opcode, symbolTable, externalReference, registers, symTabAbs);
                        objectCode = format4.process(instruction, baseUsed, baseValue);
                    }

                }

                else if (!instruction.isIsOperation()) {
                    if (instruction.getMnemonic().compare("BASE") == 0 ||
                        instruction.getMnemonic().compare("NOBASE") == 0) {
                        if (instruction.getMnemonic().compare("BASE") == 0) {
                            baseUsed = true;
                        }
                        else
                            baseUsed = false;
                    }
                    else if (instruction.getMnemonic().compare("END") == 0) {
                        if (instruction.getOperand().size() == 0)
                            firstExeAddress = startAddress;
                        else
                            firstExeAddress = symbolTable[instruction.getOperand()[0]];
                        locctr = instruction.getLocctr();
                    }
                    else if (instruction.getMnemonic().compare("WORD") == 0 ||
                             instruction.getMnemonic().compare("BYTE") == 0) {
                        if (instruction.getMnemonic().compare("WORD") == 0) {
//                            int wordLength = instruction.getOperand()[0].length();
//                            for (int i = 0; i < wordLength - 1; i++) {
//                                objectCode += '0';
//                            }
                            int temp = std::stoi(instruction.getOperand()[0]);
                            objectCode = decimalToHexa(temp);

                        }
                        else if (instruction.getMnemonic().compare("BYTE") == 0) {
                            std::string tempOperand;
                            int length = instruction.getOperand()[0].length();
                            tempOperand = instruction.getOperand()[0].substr(2, length - 3);
                            if (instruction.getOperand()[0].at(0) == 'X') {
                                objectCode += tempOperand;
                        //        std::cout << std::endl << "X is : " << objectCode << std::endl;
                            }
                            else if (instruction.getOperand()[0].at(0) == 'C') {
                                for (int i = 0; i < tempOperand.length(); i++) {
                                    std::string tempCh = "";
                                    tempCh += tempOperand.at(i);
                                    std::vector<std::string> tempVector = asciiMap[tempCh];
                                    objectCode += tempVector[1];
                                }
                            }
                        }
                    }
                }
                instruction.setObjcode(objectCode);
//                for (int i = 0; i < instruction.getErrorVector().size(); i++) {
//                    std::cout << instruction.getErrorVector()[i] << std::endl;
//                }
                if (instruction.isIsOperation() || instruction.getMnemonic().compare("BYTE") ||
                    instruction.getMnemonic().compare("WORD")) if (instruction.getErrorVector().size() == 0)
                    instructionVector.push_back(instruction);
//                std::cout << "lOOCTR " << instruction.getLocctr() << " Record: " << instruction.getRecord() <<
//                " OBJECT CODE: " << instruction.getObjcode() << std::endl;
            }
        }
        if(dummyFound){
            symbolTable.erase("dummy");
            dummyFound = false;
        }
        std::vector<std::string> temp = instruction.getErrorVector();
        instruction.setErrorVector(temp);
        print(instruction);
    }

    return instructionVector;
}
void Pass2::print(Instruction instruction) {
//    std::cout <<"***************************************************"<<std::endl;
//    std::cout << "entered print" << std::endl;
//    std::string operandsString = "";
//    operandsString += "\t";
//    for (int i = 0; i < instruction.getOperand().size(); i++) {
//        operandsString += instruction.getOperand()[i] + " ";
//    }
    std::cout << "LOCCTR: " ;
    printf("%06X",instruction.getLocctr());
    std::cout << "\t\t" << instruction.getRecord() << std::endl;
   // std::cout << "\t\t" << instruction.getLabel() << "\t" << instruction.getMnemonic() << operandsString << std::endl;
    if(instruction.getComment() != "")
        return;
    if (instruction.getObjcode().length() > 0) {
        if(instruction.isIsOperation()) {
            std::cout << "Object Code: "<<instruction.getObjcode() << std::endl;
            std::string temp = instruction.getObjcode().substr(2, 1);
            std::stringstream ss;
            ss << std::hex << temp;
            unsigned f;
            ss >> f;
            std::bitset<4> myBit(f);
            std::string mystring = myBit.to_string();
            std::string x = mystring.substr(0, 1);
            std::string b = mystring.substr(1, 1);
            std::string p = mystring.substr(2, 1);
            std::string e = mystring.substr(3, 1);
            //   std::string::size_type sz;   // alias of size_t
            int pureMnemonic = instruction.getFormat() == 4 ?  std::stoi(opcode[instruction.getMnemonic().substr(1)],
                                                                         nullptr, 16) : std::stoi(opcode[instruction.getMnemonic()], nullptr, 16);
            int actualMnemonic = std::stoi(instruction.getObjcode().substr(0, 2), nullptr, 16);
            int niBits = actualMnemonic - pureMnemonic;
            std::string binary = std::bitset<2>(niBits).to_string();
            std::string n = binary.substr(0, 1);
            std::string i = binary.substr(1, 1);
            if (instruction.getFormat() == 2) {
                n = i = p = b = x = e = "0";
            }
            std::cout << "n" << "\t" << "i" << "\t" << "x" << "\t" << "b" << "\t" << "p" << "\t" << "e" << "\t" <<
            std::endl << n << "\t" << i << "\t" << x << "\t" << b << "\t" << p << "\t" << e << std::endl;
        }else if(instruction.getMnemonic() == "BYTE" || instruction.getMnemonic() == "WORD"){
            std::cout << "Object Code: "<<instruction.getObjcode() << std::endl;
        }
    }
  //  std::cout << "entered II print" << std::endl;


    std::string errorString = "";
    for (int i = 0; i < instruction.getErrorVector().size(); i++) {
        errorString += instruction.getErrorVector()[i] + "\n";
    }
    std::cout << "Errors: " << std::endl << errorString;
    std::cout <<"==================================================================================="<<std::endl;
}
void Pass2::writeObjectProgram() {
    GenerateRecord generateRecord(make_pair(programName, startAddress), firstExeAddress, locctr, externalReference,
                                  extDefs, instructionVector);
    generateRecord.process();
}


std::string Pass2::decimalToHexa(int decimalNumber) {
    std::string hexadecimal = "";
    int quotient = decimalNumber;
    int temp;
    while (quotient != 0) {
        temp = quotient % 16;

        if (temp < 10)
            temp = temp + 48;
        else
            temp = temp + 55;

        hexadecimal += temp;
        quotient = quotient / 16;
    }
    return hexadecimal;
}
