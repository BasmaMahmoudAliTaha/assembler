#include <iostream>
#include "Parser.h"
#include "Instruction.h"
#include "Pass1.h"
#include "Pass2.h"
#include "Format.h"

using namespace std;

int main(int argc, char *argv[]) {
    freopen("out.txt","w+",stdout);
    Parser parser;
    parser.loadAppendix();
    //parser.readFile("example.txt");
    parser.readFile(argv[1]);
    parser.parseAscii("ASCII.txt");
    int fileSize = parser.getFileSize();
    Pass1 pass1(fileSize);
    for (int i = 0; i < fileSize; i++) {
        Instruction ins = parser.getInstruction();
        pass1.programFlow(ins);
    }
    std::vector<std::string> externalReference;


    Pass2 pass2(parser.getOpCode(), pass1.getInstructionVector(), pass1.getSymbolTable(), parser.getRegisters(), externalReference, parser.getAsciiMap(), pass1.getLitteralTable(), pass1.getAbsoluteTable());
    pass2.processInstructions();
    pass2.writeObjectProgram();
    return 0;
}