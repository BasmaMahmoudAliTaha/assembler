//
// Created by Microsoft on 5/6/2016.
//

#include "Format2.h"

std::string Format2::process(Instruction &instruction, bool &isBase, int &baseVal){
  std::string first, second, third;
//    std::cout<<opcode[instruction.getMnemonic()]<<std::cout;
  first = Format::getOpcode(instruction);
  third = Format2::formAddressBits(instruction, isBase, baseVal);
  second = Format2::formThirdBit(instruction);
  return first + second + third;
}


std::string Format2::formThirdBit(Instruction &instruction) {
    int temp;
    std::map<std::string, int>::iterator itr;
    itr = registers.find(instruction.getOperand()[0]);
    if (itr != registers.end()) {// find label in symbol table
        temp = itr->second;
    }
    else
        printError("Cant find register" , instruction); /// this case is not to occur
    return  Format::fromIntToHex(temp);
}

std::string Format2::formAddressBits(Instruction &instruction, bool isBase,int baseVal) {

    if(instruction.getMnemonic() == "SHIFTL" || instruction.getMnemonic() == "SHIFTR" ){
        return instruction.getOperand()[1];
    }
    if(instruction.getOperand().size() == 1)
        return "0";

    else{
        int temp;
        std::map<std::string, int>::iterator itr;
        itr = registers.find(instruction.getOperand()[1]);
        if (itr != registers.end()) {
            temp = itr->second;
        }
        else
            Format::printError("Cant find register" , instruction); /// this case is not to occur
        return  Format::fromIntToHex(temp);
    }

}
