//
// Created by Microsoft on 4/27/2016.
//

#include "Pass1.h"
#define MEMSIZE (1 << 20)

Pass1::Pass1(int fileSize)
{
  std::map <std::string, std::string> extExp;
  this -> fileSize = fileSize;
}

void Pass1::programFlow(Instruction instruction) {
  if(canBegin == 0){ // Before finding the Start
    if((instruction.getComment().size()) != 0){ // if is Comment
      std::cout << instruction.getComment() <<std::endl;
    }
    else if(instruction.getErrorVector().size()>0){ // if there is an error
      Pass1::printInstruction(instruction , -1);
      Pass1::printVectorError(instruction , instruction.getErrorVector());
      Pass1::error = true;
    }
    else if(instruction.getMnemonic() == "START") { // if start
      Pass1::canBegin = 1;
      intializeStartProgram(instruction);
    }else{
      Pass1::printInstruction(instruction , -1);
      Pass1::printError("START NOT FIND YET!!");
      Pass1::error = true;
    }
  }else if (canBegin == 1){

    if(instruction.getComment().size() != 0){
      std::cout << instruction.getComment() <<std::endl;
    }else {
      int oldlocctr = locctr;
      canAddLabel = true;
      Pass1::printInstruction(instruction, locctr);
      Pass1::checkLocctrOutOfMemory(locctr);
      if (instruction.getErrorVector().size() > 0) {
        Pass1::printVectorError(instruction, instruction.getErrorVector());
        Pass1::error = true;
        checkEndReached();
        canAddLabel = false;
        return;
      }
      if (instruction.getMnemonic() == "START") {
        Pass1::printError("MULTI START DIRECTIVE!!");
        Pass1::error = true;
        canAddLabel = false;
      }
      else if (instruction.getMnemonic() == "ORG" || instruction.getMnemonic() == "LTORG"
               || instruction.getMnemonic() == "NOBASE" || instruction.getMnemonic() == "BASE"
               || instruction.getMnemonic() == "EQU") {
        manageDirective(instruction);
      }
      else if(instruction.getMnemonic() == "RESB" || instruction.getMnemonic() == "RESW") {
        Pass1::reserveByteOrWord(instruction);
      }else if(instruction.getMnemonic() == "WORD"){
        Pass1::validateWordOperand(instruction);
      }else if(instruction.getMnemonic() == "BYTE") {
        Pass1::validateByteOperand(instruction);
      }else if(instruction.getFormat() == 2){ // format 2
        locctr += 2;
      }else if(instruction.getFormat() == 3){ // format 3
        locctr += 3;
        checkLitterals(instruction);
      }else if(instruction.getFormat() == 4){ // format 4
        locctr += 4;
        checkLitterals(instruction);
      }else if(instruction.getMnemonic() == "END"){
        Pass1::validateEndStatement(instruction);
        Pass1::handleLTORG();
      }else {
        Pass1::printError("UNDEFINED INSTRUCTION!!");
        Pass1::error = true;
        canAddLabel = false;
      }
      if(canAddLabel)
        Pass1::checkLabel(instruction.getLabel(), oldlocctr);
      instruction.setLocctr(oldlocctr);
    }
  }else{
    if(instruction.getComment().size() != 0){
      std::cout << instruction.getComment() << std::endl;
    }else {
      Pass1::printInstruction(instruction, locctr);
      Pass1::printError("INSTRUCTION AFTER END STATEMENT!!");
      Pass1::error = true;
    }
  }
  checkEndReached();
  instructionVector.push_back(instruction);
}

std::vector<Instruction> Pass1::getInstructionVector()
{
  return instructionVector;
}

std::map<std::string , int> Pass1::getSymbolTable()
{
  return symbolTable;
};

void Pass1::manageDirective(Instruction &instruction)
{
  if(instruction.getMnemonic() == "LTORG"){
    if(instruction.getLabel() != "" || instruction.getOperand().size() != 0){
      error = true;
      printError("ERROR IN LTORG!!");
    }else
      handleLTORG();
  }else if(instruction.getMnemonic() == "ORG"){
    if(instruction.getLabel() != ""){
      error = true;
      printError("ERROR IN ORG!!");
    }else
      handleORG(instruction);
  }else if(instruction.getMnemonic() == "EQU"){
    handleEQU(instruction);
  }
}

void Pass1::handleEQU(Instruction &instruction)
{
  canAddLabel = false;
  if(instruction.getLabel() == "" || instruction.getOperand()[0] == ""){
    printError("ERROR IN EQU!!");
    error = true;
  }else {
    if (instruction.isExpression()) {

      expression.setSymTableRelOrAbs(absoluteTable);
      expression.setSymTable(symbolTable);
      std::pair<bool,long long> resOfExp = expression.getValue(instruction.getOperand().at(0));
      if(resOfExp.first || std::abs(resOfExp.second) > MEMSIZE ){
        printError("ERROR IN EQU!!");
        error = true;
      }else{
        symbolTable.insert(std::make_pair(instruction.getLabel(), resOfExp.second));
       if(expression.isAbsoluteOrRel()){
         absoluteTable.insert(std::make_pair(instruction.getLabel(), true));
       }

      }

    } else if (instruction.isSimple()) {
      if (instruction.isNum()) {
        symbolTable.insert(std::make_pair(instruction.getLabel(), std::stoi(instruction.getOperand()[0],nullptr,16)));
        absoluteTable.insert(std::make_pair(instruction.getLabel(), true));
      } else if (symbolTable.find(instruction.getOperand()[0]) != symbolTable.end()) {
        symbolTable.insert(std::make_pair(instruction.getLabel(), symbolTable[instruction.getOperand()[0]]));
      }
    }else{
      printError("ERROR IN EQU!!");
      error = true;
    }
  }
}

std::map<std::string, bool> Pass1::getAbsoluteTable()
{
  return absoluteTable;
}

void Pass1::handleORG(Instruction &instruction)
{
  if (instruction.isExpression()) {

    expression.setSymTableRelOrAbs(absoluteTable);
    expression.setSymTable(symbolTable);
    std::pair<bool,long long> resOfExp = expression.getValue(instruction.getOperand().at(0));
    long long value =  resOfExp.second;
    if(resOfExp.first || value > MEMSIZE || value < 0){
      printError("ERROR IN ORG!!");
      error = true;
    }else{
      locctr = value;
    }

  } else if (instruction.isSimple()) {
    if (instruction.isNum()) {
      locctr = std::stoi(instruction.getOperand()[0], nullptr, 16);
    } else {
      if (symbolTable.find(instruction.getOperand()[0]) != symbolTable.end()) {
        locctr = symbolTable[instruction.getOperand()[0]];
      } else {
        printError("ERROR IN ORG!!");
        error = true;
      }
    }
  } else {
    printError("ERROR IN ORG!!");
    error = true;
  }
}

void Pass1::handleLTORG()
{
  for(int i = 0  ; i < litterals.size() ; i++){
    std::string litteral = litterals[i];
    GenerateObjectCodeForLitterals( litteral);
  }
  litterals.clear();
}

void Pass1::GenerateObjectCodeForLitterals( std::string litteral)
{
  if(litteral[1] == '*'){

    littab.insert(std::make_pair(litteral, locctr));
    std::vector<std::string> vs {litteral.substr(1)};
    std::vector<std::string> ev;
    Instruction newInst(false, litteral + "    BYTE    " + litteral.substr(1), vs, fromIntToHex(locctr),false,
                        "BY TE ",locctr, true, litteral,false,false,false,false,5,false,false, ev,"", false,false);
    instructionVector.push_back(newInst);
    littab.insert(std::make_pair(litteral , locctr));
    locctr += 3;
    return;
  }
  std::string temp = litteral.substr(3,litteral.size()-4);
  if(litteral[1] == 'X'){
    if(temp.size() % 2 == 1 || !Pass1::isHex(temp) || litteral.size() >= 19){
      error = true;
      printError("ERROR IN LITTERALS!!");
    }else {
      littab.insert(std::make_pair(litteral, locctr));
      std::vector<std::string> vs {litteral.substr(1)};
      std::vector<std::string> ev;
      Instruction newInst(false, litteral + "    BYTE    " + litteral.substr(1), vs, temp,false, "BY TE ",locctr,
                          true, litteral,true,false,false,false,5,false,false, ev,"", false,false);
      instructionVector.push_back(newInst);
      std::cout << Pass1::validSix(fromIntToHex(locctr))<< "   " <<  litteral + "    BYTE    " + litteral.substr(1) << std::endl;
      littab.insert(std::make_pair(litteral , locctr));
      locctr += temp.size() / 2;
    }
  }else if(litteral[1] == 'C' && litteral.size() < 20){
    //should transform ascii
    std::string str = "";
    for(int i = 0; i < temp.size(); i++){
      int x = temp[i];
      str += Pass1::fromIntToHex(x);
    }
    std::vector<std::string> vs {litteral.substr(1)};
    std::vector<std::string> ev;
    Instruction newInst(false, litteral + "    BYTE    " + litteral.substr(1), vs, str,false, "BY TE ",locctr,
                        true, litteral,true,false,false,false,5,false,false, ev,"", false, false);
    instructionVector.push_back(newInst);
    std::cout << Pass1::validSix(fromIntToHex(locctr))<< "   " << litteral + "    BYTE    " + litteral.substr(1) << std::endl;
    littab.insert(std::make_pair(litteral , locctr));
    locctr += temp.size();
  }else{
    error = true;
    printError("ERROR IN LITTERALS!!");
  }
}

std::string Pass1::validSix(std::string str){
  int strLength = str.length();
  for(int i = 0; i < 6 - strLength; i++){
    str = "0" + str;
  }
  return str;
}

std::string Pass1::fromIntToHex(int x)
{
  std::stringstream stream;
  stream << std::hex << x;
  std::string result(stream.str());
  for(int i = 0; i < result.length(); i++){
    if(result[i] > ('a' - 1) && result[i] < ('z' + 1)){
      result[i] = result[i] - 32;
    }
  }
  return result;
}

void Pass1::checkLitterals(Instruction &instruction)
{
  if(instruction.isLitteral()){
    if(littab.find(instruction.getOperand()[0]) == littab.end()){
      if(instruction.getOperand()[0] == "=*"){
          std::vector<std::string> vs ;
          vs.push_back((instruction.getOperand()[0] + std::to_string(numOFOccurences)));
          instruction.setOperand(vs);

        numOFOccurences++;
      }
     // littab.insert(std::make_pair(instruction.getOperand()[0] , 0));
      litterals.push_back(instruction.getOperand()[0]);
    }
  }
}

void Pass1::checkEndReached()
{
  Pass1::numOfInstruction ++;
  if(numOfInstruction == fileSize && !endFound) {
    Pass1::printError("END NOT FOUND IN FILE!!");
    Pass1::error = true;
  }
}

bool Pass1::isValidSyntax(std::string label)
{
  std::regex labelPattern("(_?[A-Za-z]+(\\w*))");
  if(regex_match(label,labelPattern))
    return true;
  return false;
}

void Pass1::printInstruction(Instruction instruction , int locctr)
{
  if (locctr != -1)
    printf("%06X", locctr);
  std::cout << "\t" ;
  std::cout << instruction.getRecord() ;
  std::cout << std::endl;
}

void Pass1::printVectorError(Instruction instruction ,  std::vector<std::string> s )
{
  for(std::string s1: s){
    std::cout << s1 <<std::endl;
  }
}

void Pass1::printError(std::string s )
{
  std::cout << s << std::endl;

}

std::pair<int,bool> Pass1::stringIntoInt(std::string str, int base)
{
  if(base == 16){
    bool flag = isHex(str);
    if(!flag)
      return std::make_pair(-1 , true);
  }else if(base == 10){
    bool flag = isDecimal(str);
    if(!flag)
      return
              std::make_pair(-1 ,true);
  }
  int x = 0;
  bool errorFlag = false;
  try {
    x = std::stoi(str, nullptr, base);
  }
  catch(std::invalid_argument& e){
    x = -1;
    errorFlag = true;
  }catch(std::out_of_range& e){
    x = -1;
    errorFlag = true;
  }
  return std::make_pair(x , errorFlag);
}

void Pass1::checkLabel(std::string label , int locctr)
{
  if(label != ""){ // if there is a label
    if(Pass1::symbolTable.find(label) != Pass1::symbolTable.end()) { // if found it in symbol table
      Pass1::printError("DUPLICATE LABEL!!");
      Pass1::error = true;
    }else if(isValidSyntax(label)){ // if not found in symbole table
      Pass1::symbolTable.insert(std::make_pair(label , locctr));
    }
  }
}

void Pass1::checkLocctrOutOfMemory(int locctr)
{
  if(locctr > MEMSIZE){
    Pass1::error = true;
    Pass1::printError("OUT OF MEMORY!!");
  }
}

bool Pass1::isHex(std::string operand)
{
  std::regex hex("[0-9a-fA-F]+");
  if(std::regex_match(operand, hex))
    return true;
  return false;
}

bool Pass1::isDecimal(std::string operand){
  std::regex decimal("(\\-?\\d+)");
  if(std::regex_match(operand, decimal))
    return true;
  return false;
}

void Pass1::intializeStartProgram(Instruction instruction)
{
  std::pair<int, bool> num = Pass1::stringIntoInt(instruction.getOperand()[0], 16);
  if (num.second || num.first < 0 || MEMSIZE < num.first) {
    locctr = 0;
    Pass1::printInstruction(instruction , locctr);
    Pass1::printError("ERROR IN OPERAND FIELD. LOCATION COUNTER INITIALIZED TO ZERO!!");
    Pass1::error = true;
  }else{
    locctr = num.first;
    Pass1::printInstruction(instruction , locctr);
  }
  Pass1::checkLabel(instruction.getLabel() , locctr);
  Pass1::prgName = instruction.getLabel();
  Pass1::prgStart = locctr;
}

void Pass1::reserveByteOrWord(Instruction instruction)
{
  std::pair<int , bool> num;
  if(instruction.isExpression()){
    expression.setSymTableRelOrAbs(absoluteTable);
    expression.setSymTable(symbolTable);

    std::pair<bool, long long>  resOfExp = expression.getValue(instruction.getOperand().at(0));

    if(resOfExp.first){
      Pass1::printError("INVALID Expression!!");
      Pass1::error = true;
      canAddLabel = false;
    }else{
      num = std::make_pair(true, resOfExp.second);
    }

  }else {
    num = stringIntoInt(instruction.getOperand()[0], 10);
  }
  
  if (!num.second && num.first > 0) {
    if (instruction.getMnemonic() == "RESW")
      locctr += num.first * 3;
    else
      locctr += num.first;
  }
  else {
    Pass1::error = true;
    Pass1::printError("INVALID OPERAND!!");
    canAddLabel = false;
  }
}

void Pass1::validateWordOperand(Instruction instruction)
{
  std::pair<int , bool> num;
  if(instruction.isExpression()){
    expression.setSymTableRelOrAbs(absoluteTable);
    expression.setSymTable(symbolTable);

    std::pair<bool, long long>  resOfExp = expression.getValue(instruction.getOperand().at(0));

    if(resOfExp.first){
      Pass1::printError("INVALID Expression!!");
      Pass1::error = true;
      canAddLabel = false;
    }else{
      num = std::make_pair(true, resOfExp.second);
    }

  }else {
    num = stringIntoInt(instruction.getOperand()[0], 10);
  }
  if((num.first > 9999 || num.first < -9999) || num.second){
    Pass1::printError("INVALID OPERAND!!");
    Pass1::error = true;
    canAddLabel = false;
  }else
    locctr += 3;
}

void Pass1::validateByteOperand(Instruction instruction)
{
  std::string quote = "'";
  std::string value = instruction.getOperand()[0];
  if (value.size() > 3 && (value[0] == 'X' || value[0] == 'C') && value[1] == quote[0]
      && value[value.size() - 1] == quote[0]) { // right format for value
    if (value[0] == 'X' && (value.size() < 18 && (value.size() % 2))) { // if value in hex
      locctr += (value.size() - 3) / 2;
    } else if (value[0] == 'C' && (value.size() < 19)) {
      locctr += value.size() - 3; // if value in character
    } else {
      Pass1::printError("INVALID OPERAND!!"); // more than 14 hex or 15 char or is not even in hex
      Pass1::error = true;
      canAddLabel = false;
    }
  }else{
    Pass1::printError("INVALID OPERAND!!"); // error occurs in operand
    Pass1::error = true;
    canAddLabel = false;
  }
}

void Pass1::printSymbolTable(){
  std::cout << "SYMBOL TABLE :" << std::endl;
  for(auto it = symbolTable.begin() ; it != symbolTable.end() ; it++){
    std::cout << it->first << "\t" ;
    printf("%06X\n",it->second);
  }
}

void Pass1::validateEndStatement(Instruction instruction)
{
  if ( instruction.getOperand()[0].size()==0 || Pass1::symbolTable.find(instruction.getOperand()[0])!= Pass1::symbolTable.end() ) {
    canBegin = 2;
    Pass1::endFound = true;
  }
  else{
    Pass1::printError("ERROR IN END OPERAND!!");
    Pass1::error = true;
    canAddLabel = false;
  }
}

std::map<std::string, int> Pass1::getLitteralTable()
{
  return littab;
}