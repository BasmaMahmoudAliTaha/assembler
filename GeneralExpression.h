#ifndef GeneralExpression_H
#define GeneralExpression_H
#include <regex>
#include <bits/stdc++.h>
#include <stdlib.h>

class GeneralExpression
{
private:
    std::string operand;
    bool absolute; // true if absolute, false if relative
    std::map <std::string, int> symTable;
    std::map <std::string, bool> symTableAbs;
    bool error; // there is error in the expression
    long long valueOfExp;
    std::vector<std::string> terms;//the splited version of the operands
    std::vector<std::string> operations;
    bool validAbsOrRelative();
    int minus = 0, plus = 0, labels = 0;
    bool isHex(std::string operand);
    bool pairsWithOppositeSign();
    bool digitsExp();
    bool mixBetnSymAndConst();
    int countSymbols();
    void subtractAbsLabels();
    std::vector<std::string> split(std::string str,std::regex rgx);
    void calculateExp();
    void updatePereceding(int &counter, long long &perecedingRes, int temp, int i, std::string operation);
    void evaluateValue(int counter, long long &value, long long &perecedingRes,bool &pereced, int i);
    void evaluate(bool &pereced, std::string operation,int i, long long &perecedingRes, long long &value, int temp, int &counter);
public:
    GeneralExpression();
    std::pair<bool, long long>  getValue(std::string operand);// returns a pair of.. bool weather there is error or not and the value as long long
    bool isAbsoluteOrRel(); // true if absolute false if relative
    void setSymTable(std::map <std::string, int> symTable);
    void setSymTableRelOrAbs(std::map <std::string, bool> symTableRelOrAbs);
};
#endif