//
// Created by Microsoft on 5/3/2016.
//

#include "Format.h"
#include <map>
#include "Instruction.h"
#include <sstream>


Format::Format(std::map<std::string, std::string> opcode, std::map<std::string, int> symbolTable,
               std::vector<std::string> externalReference, std::map<std::string, int> registers, std::map<std::string, bool> symTabAbs) {
    this->opcode = opcode;
    this->symbolTable = symbolTable;
    this->externalReference = externalReference;
    this->registers = registers;
    this -> symTabAbs = symTabAbs;
}

std::string Format::getOpcode(Instruction &instruction) {

    if(instruction.getFormat() == 4)
        return opcode[instruction.getMnemonic().substr(1)];
    return opcode[instruction.getMnemonic()];
}


std::string Format::formFirstTwoBits(Instruction &instruction) {
    std::string firstTwo;
    std::string opCode;
    std::string ni;
    std::string temp = Format::getOpcode(instruction);

    std::stringstream ss;
    ss << std::hex << temp;
    unsigned n;
    ss >> n;
    std::bitset<4> myBit(n);
    std::string mystring = myBit.to_string();
    opCode = mystring.substr(0, 2);


    if (instruction.isImmediate()) {
        ni = "01";
    }
    else if (instruction.isIndirect()) {
        ni = "10";
        //std::cout << "indirect  " << ni << std::endl;
    } else {
        ni = "11";
        // std::cout << "11  " << ni << std::endl;
    }

    firstTwo = opCode;
    firstTwo += ni;
    // std::cout << "FirstTwo  " << firstTwo << std::endl;
    firstTwo = Format::fromBinaryStringToHexaString(firstTwo);
    //std::cout << "FirstTwo  " << firstTwo << std::endl;
//  int st = Format::fromBinaryStringToInt(firstTwo);  //change binary string to int
//  char ch = Format::fromIntToHex(st)[0];
//  ch = toupper(ch);
//  std::string strCh = "";
//  strCh += ch;
    std::string temp1 = temp.at(0) + firstTwo;        //change int to hex
    //std::cout << "temp1 " << temp1 << std::endl;
    return temp1;
}

int Format::fromBinaryStringToInt(std::string str) {
    int st = std::stoull(str, 0, 2);
    return st;
}

std::string Format::fromIntToHex(int add) {
    std::stringstream stream;
    stream << std::hex << add;    //change int to hex
    std::string result(stream.str());
    return result;
}

void Format::printError(std::string error, Instruction &instruction) {
    std::vector<std::string> vect = instruction.getErrorVector();
    vect.push_back(error);
    instruction.setErrorVector(vect);
    //std::cout<<"err vec size"<<instruction.getErrorVector().size() << std::endl;
}

int Format::fromStringToInt(std::string str) {
    std::string::size_type sz;
    return std::stoi(str, &sz);
}

std::string Format::validAddress(int wantedSize, std::string str) {
    std::string valid;
    if (str.size() > wantedSize) {
        valid = str.substr(str.size() - 3, 3);
        return Format::stringToUpper(valid);
    }

    else {
        int numOfZeros = wantedSize - str.size();
        for (int i = 0; i < numOfZeros; i++) {
            valid += "0";

        }
        valid += str;
        return Format::stringToUpper(valid);
    }
}

std::string Format::fromBinaryStringToHexaString(std::string binary) {
    std::stringstream ss;
    ss << std::hex << binary;
    // std :: cout<< ss.str() << std::endl ;
    int st = std::stoull(ss.str(), 0, 2);

    return Format::stringToUpper(Format::fromIntToHex(st));
}

std::string Format::stringToUpper(std::string s) {
    for (int i = 0; i < s.length(); i++) {
        s[i] = toupper(s[i]);
    }
    return s;
}



