#ifndef ASSEMBLYPASS1_PASS1_H
#define ASSEMBLYPASS1_PASS1_H
#include <iostream>
#include "Instruction.h"
#include "GeneralExpression.h"
#include <bits/stdc++.h>
#include <stdio.h>
#include <regex>

class Pass1 {

private:
    int canBegin = 0;
    std::string prgName;
    bool error = false;
    bool canAddLabel = false;
    bool endFound = false;
    int numOfInstruction = 0;
    int locctr = 0;
    int fileSize;
    int prgStart = 0;
    int numOFOccurences = 0;
    std::map<std::string, bool> absoluteTable;
    std::map<std::string , int> symbolTable;
    std::map<std::string, int> littab;
    std::vector<std::string> litterals;
    bool isValidSyntax(std::string label);
    std::vector<Instruction> instructionVector;
    void printInstruction(Instruction instruction , int locctr);
    void printVectorError(Instruction instruction , std::vector<std::string> s );
    void printError(std::string s );
    void checkLabel(std::string label , int locctr);
    void checkLocctrOutOfMemory(int locctr);
    void checkEndReached();
    void handleORG(Instruction &instruction);
    void handleEQU(Instruction &instruction);
    void checkLitterals(Instruction &instruction);
    void handleLTORG();
    std::string fromIntToHex(int x);
    std::string validSix(std::string str);
    void GenerateObjectCodeForLitterals(std::string litteral);
    void manageDirective(Instruction &instruction );
    void reserveByteOrWord(Instruction instruction);
    void validateWordOperand(Instruction instruction);
    void validateEndStatement(Instruction instruction);
    void validateByteOperand(Instruction instruction);
    void intializeStartProgram(Instruction instruction);
    bool isDecimal(std::string operand);
    bool isHex(std::string operand);
    std::pair<int,bool> stringIntoInt(std::string str ,int base);
    GeneralExpression expression;
public:

    Pass1(int fileSize);
    void printSymbolTable();
    void programFlow(Instruction instruction);
    std::vector<Instruction> getInstructionVector();
    std::map<std::string , int> getSymbolTable();
    std::map<std::string, bool> getAbsoluteTable();
    std::map<std::string, int> getLitteralTable();
};


#endif //ASSEMBLYPASS1_PASS1_H